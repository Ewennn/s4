'use strict'

import Hapi from '@hapi/hapi'



import { studentController } from "./controller/studentController.mjs";
import { studentDao } from "./dao/studentDao.mjs";

const routes = [{
        method: '*',
        path: '/{any*}',
        handler: function(request, h) {
            return h.response({ message: "not found" }).code(404)
        }
    },
    {
        method: 'GET',
        path: '/student',
        handler: async function(request, h) {
            try {
                const allStudents = await studentController.findAll()
                return h.response(allStudents).code(200)
            } catch (e) {
                return h.response(e).code(400)
            }
        }
    },
    {
        method: 'GET',
        path: '/student/{no}',
        handler: async function(request, h) {
            try {
                const student = await studentController.findByNo(request.params.no)
                if (student == null) {
                    return h.response({ message: "not found" }).code(404)
                } else {
                    return h.response(student).code(200)
                }
            } catch (e) {
                return h.response({ message: "not foundd" }).code(404)
            }
        }
    },
    {
        method: 'GET',
        path: '/student/like/{name}',
        handler: async function(request, h) {
            const likedProf = await studentController.like(request.params.name)
                // console.log(likedProf);

            return h.response({ message: 'server error' }).code(500)
        }
    }
]

const server = Hapi.server({
    port: 3001,
    host: 'localhost'
});

server.route(routes);

export const init = async() => {

    await server.initialize();
    return server;
};

export const start = async() => {
    await studentDao.populate('./prisma/data/student.csv')
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};


process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});