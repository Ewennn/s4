import Prof from '../model/prof.mjs'

export const profDao = {
    //parking
    like: async(name) => {
        const response = await fetch("http://localhost:3000/prof/" + name)
        if (response.status == 200) {
            console.log(await response.json());
            // Récupération correcte depuis un fetch en get.
            return new Prof(await response.json())
        } else {
            return Promise.reject({ message: 'prof not found' })
        }
    }
}