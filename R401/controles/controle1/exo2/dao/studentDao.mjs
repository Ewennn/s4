'use strict'
import { PrismaClient } from '@prisma/client'
import csv from 'csv-parser'
import fs from 'node:fs'

import Student from '../model/student.mjs'

let prisma = new PrismaClient()
export const studentDao = {
    populate: async(fileName) => {
        try {
            await studentDao.deleteAll()
            return await new Promise((resolve, reject) => {
                fs.createReadStream(fileName)
                    .pipe(csv({
                        separator: ';',
                        mapHeaders: ({ header, index }) => header.toLowerCase().trim()
                    }))
                    .on('data', async data => {

                        const student = new Student({
                            no: data['no'],
                            firstName: data['firstname'],
                            lastName: data['lastname']
                        })

                        try {
                            await studentDao.save(student)
                        } catch (e) {
                            reject({ message: "error" })
                        }
                    })
                    .on('end', () => {
                        resolve(true);
                    });
            })
        } catch (e) {
            return Promise.reject(e)
        }
    },
    findAll: async() => {
        try {
            const students = (await prisma.student.findMany()).map(student => new Student(student))
            return students
        } catch (e) {
            return Promise.reject(e)
        }
    },
    save: async(student) => {
        try {
            const res = await prisma.student.create({ data: student })
            return new Student(res)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    deleteAll: async() => {
        try {
            return await prisma.student.deleteMany({})
        } catch (e) {
            return Promise.reject(e)
        }
    },
    findByNo: async(no) => {
        try {
            const student = await prisma.student.findUnique({ where: { no: no } })
            return student == null ? null : new Student(student)
        } catch (e) {
            return Promise.reject(e)
        }
    }
}