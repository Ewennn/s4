'use strict'
import { studentDao } from "../dao/studentDao.mjs";
import { profDao } from "../dao/profDao.mjs";
export const studentController = {
    findAll: async() => {
        try {
            return await studentDao.findAll()
        } catch (e) { return Promise.reject({ message: "error" }) }
    },
    findByNo: async(no) => {
        try {
            return await studentDao.findByNo(no)
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },
    like: async(name) => {
        try {
            const prof = await profDao.like(name)
            console.log(prof);
            return prof
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    }
}