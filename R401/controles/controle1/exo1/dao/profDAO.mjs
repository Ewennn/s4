'use strict'


import Prof from '../model/prof.mjs'

let profs = [
    new Prof({name:"JoJo",like:0, unlike:100}),
    new Prof({name: "Raoul",like:4, unlike:0})]

export const profDAO = {
    findAll :  () => profs,
    findByName : (name) => profs.find((prof)=>prof.name == name),
    likeByName: (name) => {
        const profInBD = profDAO.findByName(name)
        if (profInBD!=null) {
            profInBD.like++
        }
        return profDAO.findByName(name)
    },
    unlikeByName: (name) => {
        const profInBD = profDAO.findByName(name)
        if (profInBD!=null) {
            profInBD.unlike++
        }
        return profDAO.findByName(name)
    },
}