import { init } from '../server.mjs';

import chai from 'chai'
import chaiHttp from 'chai-http'

let should = chai.should()

chai.use(chaiHttp)

describe('prof', () => {
    let server;

    beforeEach(async() => {
        server = await init();
    });

    afterEach(async() => {
        await server.stop();
    });

    it('get all ok', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/prof'
        })
        chai.expect(res.statusCode).to.equal(200)
        chai.expect(res.result).to.be.eql([{
                name: "JoJo",
                like: 0,
                unlike: 100
            },
            {
                name: "Raoul",
                like: 4,
                unlike: 0
            }
        ])
    })

    it('get one ok', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/prof/JoJo'
        })
        chai.expect(res.statusCode).to.equal(200)
        chai.expect(res.result).to.be.eql({
            name: "JoJo",
            like: 0,
            unlike: 100
        })
    })

    it('get not found', async() => {
        const res = await server.inject({
            method: 'get',
            url: '/prof/Jo'
        })
        chai.expect(res.statusCode).to.equal(404)
        chai.expect(res.result).to.be.eql({
            "message": "not found"
        })
    })

    it('put like prof ok', async() => {
        const res = await server.inject({
            method: 'put',
            url: '/prof/like/JoJo',
            payload: { name: "Jojo" }
        })
        chai.expect(res.statusCode).to.equal(200)
        chai.expect(res.result).to.be.eql({
            name: "JoJo",
            like: 1,
            unlike: 100
        })
    })

    it('put like prof ko', async() => {
        const res = await server.inject({
            method: 'put',
            url: "/prof/like/Jo",
            payload: { name: "Jo" }
        })
        chai.expect(res.statusCode).to.equal(400)
        chai.expect(res.result).to.be.eql({
            "message": "error"
        })
    })

    it('put unlike prof ok', async() => {
        const res = await server.inject({
            method: 'put',
            url: "/prof/unlike/JoJo",
            payload: { name: "Jojo" }
        })
        chai.expect(res.statusCode).to.equal(200)
        chai.expect(res.result).to.be.eql({
            "name": "JoJo",
            "like": 1,
            "unlike": 101
        })
    })

    it('put unlike prof ko', async() => {
        const res = await server.inject({
            method: 'put',
            url: "/prof/unlike/Jo",
            payload: { name: "Jo" }
        })
        chai.expect(res.statusCode).to.equal(400)
        chai.expect(res.result).to.be.eql({
            "message": "error"
        })
    })
})

describe('default route', () => {
    let server;

    beforeEach(async() => {
        server = await init();

    });

    afterEach(async() => {
        await server.stop();
    });

    it('expected default route ', async() => {

        const res = await server.inject({
            method: 'get',
            url: '/api/v1/brasserie'
        });
        chai.expect(res.statusCode).to.equal(404);
        chai.expect(res.result).to.be.eql({ message: 'not found' })
    })
})