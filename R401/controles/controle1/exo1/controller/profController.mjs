'use strict'
import {profDAO} from "../dao/profDAO.mjs";
export const profController = {
    findAll : () => profDAO.findAll(),
    findByName : (name) =>profDAO.findByName(name),
    like: (name)=>profDAO.likeByName(name),
    unlike: (name)=>profDAO.unlikeByName(name)
}