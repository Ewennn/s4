'use strict'

import Hapi from '@hapi/hapi'
import Joi from 'joi'

import Inert from '@hapi/inert'
import Vision from '@hapi/vision';
import HapiSwagger from 'hapi-swagger';

import {profController} from "./controller/profController.mjs";

const joiProf = Joi.object({
        name: Joi.string().required().description("name must be unique"),
        like: Joi.number().min(0).required().description("number of like"),
        unlike: Joi.number().min(0).required().description("number of unlike")
}).description('Prof')

const joiProfs = Joi.array().items(joiProf).description("A collection of Prof")

const notFound = Joi.object({
    message: "not found"
})

const errorMessage = Joi.object({
    message: "error"
})

const swaggerOptions = {
    info: {
        title: "L'API des utilisateurs",
        version: '1.0.0',
    }
};

const routes =[
    {
        method: '*',
        path: '/{any*}',
        handler: function (request, h) {
            return h.response({message: "not found"}).code(404)
        }
    },
    {
        method: 'GET',
        path: '/prof',
        options: {
            description: 'Get Prof list',
            notes: 'Returns an array of profs',
            tags: ['api'],
            response: {
                status: {
                    200 : joiProfs
                    }
                }
            },
        handler: (request, h) => {
                const  profs = profController.findAll()
                return h.response(profs).code(200)
        }
    },
    {
        method: 'GET',
        path: '/prof/{name}',
        options: {
            description: 'Get Prof',
            notes: 'Returns a prof or un an error message',
            tags: ['api'],
            validate: {
                params: Joi.object({
                    name : Joi.string()
                        .required()
                        .description('the name for the prof'),
                })
            },
            response: {
                status: {
                    200 : joiProf,
                    404 : notFound
                }
            }
        },
        handler:  (request, h) => {
            const prof = profController.findByName(request.params.name)
            if (prof == null)
                return h.response({message: 'not found'}).code(404)
            else
                return h.response(prof).code(200)
        }
    },
    {
        method: 'PUT',
        path: '/prof/like/{name}',
        options: {
            description: 'Like Prof',
            notes: 'Returns a prof or un an error message',
            tags: ['api'],
            validate: {
                params: Joi.object({
                    name : Joi.string()
                        .required()
                        .description('the name for the prof'),
                })
            },
            response: {
                status: {
                    200 : joiProf,
                    400 : errorMessage
                }
            }


        },
        handler: async (request, h) => {
                const prof =profController.like(request.params.name)
                if (prof != null )
                    return h.response(prof).code(200)
                else
                    return h.response({message: 'error'}).code(400)

        }
    },
    {
        method: 'PUT',
        path: '/prof/unlike/{name}',
        options: {
            description: 'Unlike Prof',
            notes: 'Returns a prof or un an error message',
            tags: ['api'],
            validate: {
                params: Joi.object({
                    name : Joi.string()
                        .required()
                        .description('the name for the prof'),
                })
            },
            response: {
                status: {
                    200 : joiProf,
                    400 : errorMessage
                }
            }


        },
        handler: async (request, h) => {
            const prof =profController.unlike(request.params.name)
            if (prof != null )
                return h.response(prof).code(200)
            else
                return h.response({message: 'error'}).code(400)

        }
    }
]

const server = Hapi.server({
    port: 3000,
    host: 'localhost'
});

server.route(routes);

export const init = async () => {

    await server.initialize();
    return server;
};

export  const start = async () => {
    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions
        }
    ]);
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});


