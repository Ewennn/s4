import { PrismaClient } from '@prisma/client'
import { DonDao } from './donDao.mjs'
const prisma = new PrismaClient()


export const DonnateurDao = {
    //Supprime tous les donnateurs
    deleteAll: async() => {
        try {
            return await prisma.donnateur.deleteMany({})
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },
    //Ajoute un donnateur et son don
    //renvoie au format {
    //             nom : nomDonnateur,
    //             dons: [{ identifiant: 1, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }]
    //             }
    //réalisable avec plusieurs requêtes
    add: async(nomDonnateur, nomAssociation, montant) => {
        try {
            const response = await prisma.donnateur.create({
                data: {
                    nom: nomDonnateur,
                    dons: {
                        create: [{
                            montant: montant,
                            nomAssociation: nomAssociation
                        }]
                    }
                },
                include: {
                    dons: true
                }
            })
            if (response == null) {
                return null
            }
            return response
        } catch (e) {
            // console.log(e);
            return Promise.reject({ message: "error" })
        }
    },
    //Peremet de réaliser un nouveau don pour une association
    //renvoie au format {
    //             nom : nomDonnateur,
    //             dons: [{ identifiant: 1, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }]
    //             }
    //réalisable avec plusieurs requêtes
    donate: async(nomDonnateur, nomAssociation, montant) => {
        try {
            const exist = await prisma.association.findUnique({ where: { nom: nomAssociation } })
            if (exist != null) {
                return exist
            }
            const response = prisma.association.create({
                data: {
                    nom: nomAssociation,
                    dons: {
                        create: [{
                            montant: montant,
                            nomDonnateur: nomDonnateur
                        }]
                    }
                },
                include: {
                    dons: true
                }
            })
            if (response == null) {
                return null
            }
            return response
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },

}