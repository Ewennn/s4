import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()


export const DonDao = {

    //Supprime tous les dons
    deleteAll: async() => {
        try {
            return await prisma.don.deleteMany({})
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },
    //donne pour une association la somme des montants de tous les dons pour cette association
    getDonationsSum: async(nomAssociation) => {
        try {
            const response = await prisma.association.findUnique({
                where: {
                    nom: nomAssociation
                },
                include: {
                    dons: true
                }
            })
            if (response == null) {
                return null
            }
            let sum = 0

            response.dons.forEach(it => {
                try {
                    sum += parseFloat(it.montant)
                } catch (e) {
                    return Promise.reject({ message: "error" })
                }

            });
            return sum

        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },
    //Renvoie pour un donnateur la liste de ses dons au format
    //[{nomAssociation : XXX, montant : "YYY"}, ...]
    getDonationsByDonator: async(nomDonnateur) => {
        try {
            const response = await prisma.donnateur.findUnique({
                where: {
                    nomDonnateur: nomDonnateur
                },
                include: {
                    dons: true
                }
            })
            if (response == null) {
                return null
            }
            return response
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    }
}