import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()


export const AssociationDao = {
    //Supprime tous les associations
    deleteAll: async() => {
        try {
            return await prisma.association.deleteMany({})
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },
    //Ajoute une association
    add: async(nomAssociation) => {
        try {
            const exist = await prisma.association.findUnique({ where: { nom: nomAssociation } })
            if (exist != null) {
                return exist
            }

            const response = await prisma.association.create({
                data: {
                    nom: nomAssociation
                }
            })
            if (response == null) {
                return null
            }
            return response
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },



    //Renvoie toutes les associations (les dons ne sont pas retournés)
    findAll: async() => {
        try {
            const response = await prisma.association.findMany({})
            if (response == null) {
                return null
            }
            return response
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },

    //Renvoie une association connue par son nom (les dons ne sont pas retournés)
    findByName: async(nom) => {
        try {
            const response = await prisma.association.findUnique({
                where: {
                    nom: nom
                }
            })

            if (response == null) {
                return null
            }
            return response
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    }
}