import chai from 'chai'
import deepEqualInAnyOrder from 'deep-equal-in-any-order';
chai.use(deepEqualInAnyOrder);

import { DonnateurDao } from "../dao/donnateurDao.mjs"
import { AssociationDao } from "../dao/associationDao.mjs"
import { DonDao } from "../dao/donDao.mjs"

describe('Donnateur', () => {
    beforeEach(async() => {
        await DonnateurDao.deleteAll()
        await AssociationDao.deleteAll()
        await DonDao.deleteAll()
    })

    it('deleteAll', async() => {
        const res = await DonnateurDao.deleteAll()

        chai.expect(res).to.be.eql({ count: 0 })
    })

    it('add1', async() => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)

        const montant = 100.0
        const nomDonnateur = "Donnateur 1"
        res = await DonnateurDao.add(nomDonnateur, a1.nom, montant)
        chai.expect(res).to.be.eql({
            nom: nomDonnateur,
            dons: [{ identifiant: res.dons[0].identifiant, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }]
        })
    })

    it('add2', async() => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)

        const montant = 100.0
        const nomDonnateur = "Donnateur 1"
        res = await DonnateurDao.add(nomDonnateur, a1.nom, montant)

        chai.expect(res).to.deep.equalInAnyOrder({
            nom: nomDonnateur,
            dons: [{ identifiant: res.dons[0].identifiant, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }]
        })

        try {
            res = await DonnateurDao.add(nomDonnateur, a1.nom, montant)
        } catch (e) { chai.expect(e).to.be.eql({ message: "error" }) }
    })

    it('add3', async() => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)

        const montant = null
        const nomDonnateur = "Donnateur 1"

        try {
            res = await DonnateurDao.add(nomDonnateur, a1.nom, montant)
        } catch (e) { chai.expect(e).to.be.eql({ message: "error" }) }
    })

    it('add4', async() => {
        const a1 = { nom: "a1" }

        const montant = 100.0
        const nomDonnateur = "Donnateur 1"

        try {
            let res = await DonnateurDao.add(nomDonnateur, a1.nom, montant)
        } catch (e) { chai.expect(e).to.be.eql({ message: "error" }) }
    })

    it('donate 1', async() => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)
        const montant = 100.0
        const nomDonnateur = "Donnateur 1"
        res = await DonnateurDao.add(nomDonnateur, a1.nom, montant)
        const id = res.dons[0].identifiant
        chai.expect(res).to.deep.equalInAnyOrder({
            nom: nomDonnateur,
            dons: [{ identifiant: id, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }]
        })
        const montant2 = 50.0
        res = await DonnateurDao.donate(nomDonnateur, a1.nom, montant2)
        chai.expect(res).to.deep.equalInAnyOrder({
            nom: nomDonnateur,
            dons: [{ identifiant: id, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }, { identifiant: id + 1, montant: montant2, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }]
        })

    })
    it('donate 2', async() => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)

        const a2 = { nom: "a2" }
        res = await AssociationDao.add(a2.nom)
        chai.expect(res).to.be.eql(a2)

        const montant = 100.0
        const nomDonnateur = "Donnateur 1"
        res = await DonnateurDao.add(nomDonnateur, a1.nom, montant)
        const id = res.dons[0].identifiant
        chai.expect(res).to.deep.equalInAnyOrder({
            nom: nomDonnateur,
            dons: [{ identifiant: id, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }]
        })
        const montant2 = 50.0
        res = await DonnateurDao.donate(nomDonnateur, a2.nom, montant2)
        chai.expect(res).to.deep.equalInAnyOrder({
            nom: nomDonnateur,
            dons: [{ identifiant: id, montant: montant, nomAssociation: a1.nom, nomDonnateur: nomDonnateur }, { identifiant: id + 1, montant: montant2, nomAssociation: a2.nom, nomDonnateur: nomDonnateur }]
        })

    })
})