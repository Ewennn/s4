import chai from 'chai'
import {AssociationDao} from "../dao/associationDao.mjs"


describe('Association', () => {
    beforeEach(async () => {
        await AssociationDao.deleteAll()
        }
    )


    it('deleteAll 1', async () => {
        const res = await  AssociationDao.deleteAll()

        chai.expect(res).to.be.eql({ count: 0 })
    })

    it('findAll 1', async () => {
        const res = await AssociationDao.findAll()
        chai.expect(res).to.be.eql([])
    })

    it('findAll 2', async () => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)
        res = await AssociationDao.findAll()
        chai.expect(res).to.be.eql([a1])
    })


    it('findByName 1', async () => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)

        res = await  AssociationDao.findByName(a1.nom)
        chai.expect(res).to.be.eql(a1)
    })

    it('findByName 2', async () => {
        const res = await  AssociationDao.findByName("Pas la")
        chai.expect(res).to.be.eql(null)
    })

    it('add 1', async () => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)
    })

    it('add 2', async () => {
        const a1 = { nom: "a1" }
        let res = await AssociationDao.add(a1.nom)
        chai.expect(res).to.be.eql(a1)
        try {
            await AssociationDao.add(a1.nom)
        } catch(e) {chai.expect(e).to.be.eql({message: "error"})}

    })

    it('add 3', async () => {
        const a1 = { nom: "a1" }
        try {
            await AssociationDao.add(a1)
        } catch(e) {chai.expect(e).to.be.eql({message: "error"})}

    })
})
