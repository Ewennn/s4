R4.01 CC2 modèles
==================
Durée 1h/1h20 Tiers temps

Tous les jeux de tests ne sont pas fournis.

Création du modèle 
-------------------
Une association reçoit des dons donnés par des donnateurs 

Une association est caractérisée par son nom (une string).
Un donnateur est caractérisé par son nom (une string).
Un don est caractérisé par un son identifiant, un entier autoincrémentant 
et un montant (un flottant).

Un donnateur fait au moins un don, et peut en faire plusieurs.
Un don ne peut-être fait qu'à une association
Une association peut ne pas avoir de dons mais peut en recevoir plusieurs. 



Vous trouverez-ici la documentation :  [prisma] https://www.prisma.io/docs/concepts/components/prisma-schema 

Vous devez au final obtenir le schéma proche du suivant : 

CREATE TABLE IF NOT EXISTS "Association" (
    "nom" TEXT NOT NULL PRIMARY KEY
);
CREATE TABLE IF NOT EXISTS "Donnateur" (
    "nom" TEXT NOT NULL PRIMARY KEY
);
CREATE TABLE IF NOT EXISTS "Don" (
    "identifiant" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    "montant" REAL NOT NULL,
    "nomDonnateur" TEXT NOT NULL,
    "nomAssociation" TEXT NOT NULL,
CONSTRAINT "Don_nomDonnateur_fkey" FOREIGN KEY ("nomDonnateur") REFERENCES "Donnateur" ("nom") ON DELETE CASCADE ON UPDATE CASCADE,
CONSTRAINT "Don_nomAssociation_fkey" FOREIGN KEY ("nomAssociation") REFERENCES "Association" ("nom") ON DELETE CASCADE ON UPDATE CASCADE
);

DAO
---
Pour ce qui suit des jeux de tests sont fournis mais ne couvrent pas l'ensemble des besoins.
Seule la partie dao sera évaluée et non la partie tests.
# AssociationDao
Compléter assocaitionDao, le test  est fourni.
# DonnateurDao
Compléter donnateurDao, le test est fourni.
# DonDao
Compléter donDao, le test n'est pas fourni

