import Hapi from '@hapi/hapi'
import Inert from '@hapi/inert'
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { Server } from 'socket.io';

const routes =[
    {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true,
                index: true,
            }
        }
    }
]


const server =  Hapi.server({
    port: 3000,
    host: 'localhost',
    routes: {
        files: {
            relativeTo: path.join(__dirname, 'public')
        }
    }

});

const io = new Server(server.listener);


export const init = async () => {

    await server.initialize();
    return server;
};

let content = ""
export  const start = async () => {
    await server.register([
        Inert
    ]);

    server.route(routes);
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    //socket
    io.on('connection', (socket) => {
        socket.emit('init-data', content);
        socket.on("on-editor-input", (data) => {
                console.log("on-editor-input" + data);
                content=data;
                socket.broadcast.emit('broadcast-data',data);
            }
        )
    });


    return server;
};


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});