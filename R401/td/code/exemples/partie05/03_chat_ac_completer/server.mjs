import Hapi from '@hapi/hapi'
import Inert from '@hapi/inert'
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { Server } from 'socket.io';

const routes =[
    {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true,
                index: true,
            }
        }
    }
]


const server =  Hapi.server({
    port: 3000,
    host: 'localhost',
    routes: {
        files: {
            relativeTo: path.join(__dirname, 'public')
        }
    }

});

const io = new Server(server.listener);


export const init = async () => {

    await server.initialize();
    return server;
};

// Global variables to hold all usernames and rooms created
//usernames = {Jojo:'JoJo', Paul:'Paul'}
let usernames = {};
let rooms = [
    { name: "global", creator: "Anonymous" },
    { name: "chess", creator: "Anonymous" },
];
export  const start = async () => {
    await server.register([
        Inert
    ]);

    server.route(routes);
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    //socket


    io.on("connection",  (socket) => {
        console.log(`User connected to server.`);

        socket.on("createUser",  (username) => {
            socket.username = username;
            usernames[username] = username; //creation d'un attribut
            socket.currentRoom = "global";
            socket.join("global");
            console.log(`User ${username} created on server successfully.`);
            socket.emit("updateChat", "INFO", "You have joined global room");
            socket.broadcast
                .to("global")
                .emit("updateChat", "INFO", username + " has joined global room");
            io.sockets.emit("updateUsers", usernames);
            socket.emit("updateRooms", rooms, "global");
        });

        socket.on("sendMessage",  (data) => {
            //TODO
        });

        socket.on("createRoom", (room) => {
            //TODO
        });

        socket.on("updateRooms",  (room) => {
            //TODO
        });

        socket.on("disconnect",  () => {
            //TODO
        });
    });


    return server;
};


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});