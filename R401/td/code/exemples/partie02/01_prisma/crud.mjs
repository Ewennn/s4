"use strict"
//importation du client
import { PrismaClient } from '@prisma/client'
const prisma = new PrismaClient()

class User {
    login
    password

    constructor(obj) {
        //declare et instancie les attribut en recopiant ceux de obj
        Object.assign(this, obj)
    }
}
//recherche de 'user1'
let user1 = await prisma.user.findUnique({
    where: {
        login: 'user1'
    }})
//user1 non trouvé
if (user1 != null) {
    const deleteUser = await prisma.user.delete({
        where: {
            login: 'user1',
        }
    })
    console.log(`${deleteUser} a été supprimé`)
}

console.log(user1) //user1 null
//création de user1
user1 = new User({login: 'user1', password: 'pass1'})
//sauvegarde de user1
const createUser = await prisma.user.create({ data: user1 })
console.log(user1, createUser)
//User { login: 'user1', password: 'pass1' } { login: 'user1', password: 'pass1' }
console.log(new User(createUser))
//User { login: 'user1', password: 'pass1' }

//mofification du mot de passe
const updateUser = await prisma.user.update({
    where: {
        login: 'user1',
    },
    data: {
        password: 'passpass',
    },
})

console.log(createUser, new User(createUser))
