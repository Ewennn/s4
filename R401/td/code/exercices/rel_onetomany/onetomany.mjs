'use strict'

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

function makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
}
const email = makeid(5)

await prisma.post.deleteMany({
    where: {
        id: {
            gt: 0
        }
    }
})

await prisma.user.deleteMany({
    where: {
        id: {
            gt: 0
        }
    },
})

await prisma.user.create({
    data: {
        email: email + "@test.fr",
    }
})

await prisma.post.create({
    data: {
        userEmail: email + "@test.fr",
        comment: makeid(54)
    }
})
await prisma.post.create({
    data: {
        userEmail: email + "@test.fr",
        comment: makeid(54)
    }
})

const response = await prisma.user.findMany({
    where: {
        id: {
            gt: 0
        }
    },
    include: {
        posts: true
    }
})
console.log(response[0]);