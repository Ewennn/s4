'use strict'

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

function makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
}
const email = makeid(5)

await prisma.profile.deleteMany({
    where: {
        id: {
            gt: 1
        }
    }
})

await prisma.user.deleteMany({
    where: {
        id: {
            gt: 1
        }
    },
})

await prisma.user.create({
    data: {
        email: email + "@test.fr",
    }
})

await prisma.profile.create({
    data: {
        userEmail: email + "@test.fr"
    }
})

console.log(await prisma.user.findMany({
    where: {
        id: {
            gt: 1
        }
    },
    include: {
        profile: true
    }
}));