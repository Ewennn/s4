'use strict'

import Hapi from '@hapi/hapi'
import  Joi from 'joi'
import Inert from '@hapi/inert'
import Vision from '@hapi/vision'
import HapiSwagger from 'hapi-swagger'

import {userController} from "./controller/userController.mjs";

const swaggerOptions = {
    info: {
        title: "L'API des utilisateurs",
        version: '1.0.0',
    }
}

const joiUserModel = Joi.object({
        login: Joi.string().required().description("login must be unique"),
        password: Joi.string().required().description("must be non-empty")
})
const joiUsers = Joi.array().items(joiUserModel).description("Collection of User")
const joiError = Joi.object({
    message: Joi.string().required().description("Error message")
}).description("Error message object")
const joiTest = Joi.object({
    args: Joi.string().required()
})

const routes =[
    {
        method: '*',
        path: '/{any*}',
        handler: function (request, h) {
            return h.response({message: "not found"}).code(404)
        }
    },
    {
        method: 'GET',
        path: '/user',
        options: {
            description: 'Get User list',
            notes: 'Retruens an array of user',
            tags: ['api'],
            response: {
                status: {
                    200: joiUsers,
                    400: joiError,
                    500: joiTest
                }
            }
        },
        handler: async (request, h) => {
            //le message renvoyé et le code hhtp
            try {
                const  users = await userController.findAll()
                return h.response(users).code(200)
            } catch (e) {
                return h.response(e).code(400)
            }
        }
    },
    {
        method: 'GET',
        //une route avec un parametre
        //utilisable avec request.params.login
        path: '/user/{login}',
        handler: async (request, h) => {
            try {
                const user = await userController.findByLogin(request.params.login)
               if (user == null)
                    return h.response({message: 'not found'}).code(404)
                else
                    return h.response(user).code(200)
            } catch (e) {
                return h.response({message: 'error'}).code(404)
            }
        }
    },
    {
        method: 'POST',
        path: '/user',
        options: {
            validate: {
                payload: joiUserModel
            }
        },
        handler: async (request, h) => {
            try {
                //Le body est accessible via request.payload
                const userToAdd = request.payload
                const user = await userController.add(userToAdd)
                return h.response(user).code(201)
            } catch (e) {
                return h.response({message: 'error'}).code(400)
            }
        }
    },
    {
        method: 'DELETE',
        path: '/user/{login}',
        handler: async (request, h) => {
            try {
                const user = await userController.deleteByLogin(request.params.login)
                return h.response(user).code(200)
            } catch (e) {
                return h.response({message: 'not found'}).code(404)
            }
        }
        },
    {
        method: 'PUT',
        path: '/user/{login}',
        options: {
            validate: {
                payload: joiUserModel
            }
        },
        handler: async (request, h) => {
            try {
                const user = await userController.update(request.params.login, request.payload)
                return h.response(user).code(200)
            } catch (e) { return h.response({message: 'not found'}).code(400)}
        }
    }
]

const server = Hapi.server({
    port: 3000,
    host: 'localhost'
});

server.route(routes);

export const init = async () => {

    await server.initialize();
    return server;
};

export  const start = async () => {
    await server.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerOptions,
        }
    ])
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});


