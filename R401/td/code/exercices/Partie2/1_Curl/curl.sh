url='https://jsonplaceholder.typicode.com/posts'

curl -k $url

echo -ne '\n==========\n'

curl -k "${url}/1"

echo -ne '\n==========\n'

curl -k -i "${url}/-1"

echo -ne '\n==========\n'

curl -k -X -i DELETE "${url}/1"

echo -ne '\n==========\n'

curl -k -X -i DELETE "${url}/1000"

echo -ne '\n==========\n'

data='{"userId": 1,"id": 10,"title": "sunt aut ","body": "quia et"}'
curl -k -v -H 'Content-type: application/json' -d $data PUT "${url}/10"
