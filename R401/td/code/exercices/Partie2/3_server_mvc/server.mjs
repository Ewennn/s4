'use strict';
import Hapi from '@hapi/hapi';
import Joi from 'joi';
import { userController } from "./controller/userController.mjs";

const joiUser = Joi.object({
    login: Joi.string().required(),
    password: Joi.string().required()
})

const routes = [{
        method: 'GET',
        path: '/user',
        handler: async(request, h) => {
            //le message renvoyé et le code http
            return h.response(await userController.findAll()).code(200)
        }
    },
    {
        method: 'GET',
        //une route avec un parametre
        //utilisable avec request.params.login
        path: '/user/{login}',
        handler: async(request, h) => {
            const user = await userController.findByLogin(request.params.login)
            if (user != null)
                return h.response(user).code(200)
            else
                return h.response({ message: 'not found' }).code(404)
        }
    },
    {
        method: 'POST',
        path: '/user',
        options: {
            validate: {
                payload: joiUser
            }
        },
        handler: async(request, h) => {
            //Le body est accessible via request.payload
            try {
                // const { error } = joiUser.validate(request.payload)
                // if (error != null) {
                //     throw new Error("Donnée non valide")
                // }

                const userToAdd = request.payload
                console.log(userToAdd);
                const user = await userController.add(userToAdd)
                if (user != null)
                    return h.response(user).code(201)
                else
                    throw new Error("Body vide")
            } catch (e) {
                console.log(e.message)

                return h.response({
                    "statusCode": 500,
                    "error": "Bad Request",
                    "message": "Invalid request payload input"
                }).code(500)
            }
        }
    },
    {
        method: 'DELETE',
        path: '/user/{login}',
        handler: async(request, h) => {
            const user = await userController.deleteByLogin(request.params.login)
            if (user != null)
                return h.response(user).code(200)
            else
                return h.response({
                    "statusCode": 404,
                    "error": "Bad Request",
                    "message": "Invalid request payload input"
                }).code(404)
        }
    },
    {
        method: 'PUT',
        path: '/user/{login}',
        options: {
            validate: {
                payload: joiUser
            }
        },
        handler: async(request, h) => {
            try {
                // const { error } = joiUser.validate(request.payload)
                // if (error != null) {
                //     throw new Error("Donnée non valide")
                // }
                const user = await userController.update(request.params.login, request.payload)
                if (user != null)
                    return h.response(user).code(200)
                else
                    throw new Error("erreru")
            } catch (e) {
                return h.response({
                    "statusCode": 400,
                    "error": "Bad Request",
                    "message": "Invalid request payload input"
                }).code(400)
            }
        }
    }
]
const server = Hapi.server({
    port: 3000,
    host: 'localhost'
});
server.route(routes);
export const init = async() => {
    await server.initialize();
    return server;
};
export const start = async() => {
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    return server;
};
process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});