"use strict"

import Hapi from '@hapi/hapi'

class User {
    login
    password
    constructor(obj) {
        Object.assign(this, obj)
    }
}

class Users {
    users = []
}

const model = new Users()

const userDao = {
    findAll : () => model.users,
    findByLogin : (login) => {
        const users = model.users.filter((user) => user.login == login)
        return users.length != 0 ? users[0] : null
    },
    deleteByLogin : (login) => {
        const user = userDao.findByLogin(login)
        if(user == null) return null
        model.users = model.users.filter(user => user.login != user.login)
        return user
    },
    add : (user) => {
        const userByLogin = userDao.findByLogin(user.login)
        if (userByLogin != null) {
            return null
        }
        model.users.push(user)
        return user
    },
    update : (login, user) => {
        const userByLogin = userDao.findByLogin(login)
        if (userByLogin == null) {
            return null
        }
        userDao.deleteByLogin(login)
        userDao.deleteByLogin(user.login)
        userDao.add(user)
        return user
    },
}

const init = async () => {
    const server = Hapi.server({
        port: 3000,
        host: 'localhost'
    });

    server.route([
        {
            method: 'GET',
            path: '/',
            handler: (request, h) => {
                return "Welcome to my first rest API !"
            }
        },
        {
            method: 'GET',
            path: '/user',
            handler: (request, h) => {
                return h.response(userDao.findAll()).code(200)
            }
        },
        {
            method: 'GET',
            path: '/user/{login}',
            handler: (request, h) => {
                const user = userDao.findByLogin(request.params.login)
                if (user != null) return h.response(user).code(200)
                else return h.response({message: 'not found'}).code(404)
            }
        },
        {
            method: 'POST',
            path: '/user',
            handler: (request, h) => {
                const userToAdd = request.payload
                const user = userDao.add(userToAdd)
                if (user != null) return h.response(user).code(201)
                else return h.response({message: 'already exist'}).code(400)
            }
        },
        {
            method: 'DELETE',
            path: '/user/{login}',
            handler: (request, h) => {
                const user = userDao.deleteByLogin(request.params.login)
                if (user != null) return h.response(user).code(200)
                else h.response({message: 'not found'}).code(404)
            }
        },
        {
            method: 'PUT',
            path: '/user/{login}',
            handler: (request, h) => {
                const user = userDao.update(request.params.login, request.payload)

                if (user != null) return request.response(user).code(200)
                else return request.response({message: 'not found'}).code(400)
            }
        }
    ]);
    await server.start()
    console.log('Server running on %s', server.info.uri);
}

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1)
});
init()