"use strict"

import { PrismaClient } from "@prisma/client"

const prisma = new PrismaClient()

const degreesToRadians = (degrees) => {
    return degrees * (Math.PI / 100)  
} 

class Parking {
    identifiant
    nom
    ouvert
    horodatage
    nombreDePlacesDisponibles
    latitude
    longitude

    constructor(obj) {
        //declare et instancie les attribut en recopiant ceux de obj
        Object.assign(this, obj)
    }

    distance(latitude, longitude) {
        return Math.acos(
            Math.sin(degreesToRadians(this.latitude)) * Math.sin(degreesToRadians(latitude)) +
            Math.cos(degreesToRadians(this.latitude)) * Math.cos(degreesToRadians(latitude)) *
            Math.cos(degreesToRadians(this.longitude - longitude)) * 6371
        )
    }
}

const parkingDao = {
    findParkings : async () => (await prisma.parking.findMany()).map(p => new Parking(p)),
    findParkingByIdentifiant : async (id) => {
        const park = await prisma.parking.findUnique({where: {identifiant: id}})
        return park != null ? new Parking(park) : null
    },
    findParkingByNom : async (nom) => {
        const park = await prisma.parking.findUnique({where: {nom: nom}})
        return park != null ? new Parking(park) : null
    },
    deleteParkings: async () => await prisma.parking.deleteMany(),
    addParking : async (parking) => {
        // try {
        //     const park = await prisma.parking.create({data: parking})
        //     return new Parking(park)
        // } catch (e) {
        //     return Promise.reject(e)
        // }
        if ((await parkingDao.findParkings()).filter(p => p.identifiant == parking.identifiant).length == 0) {
            await prisma.parking.create({data: parking})
            return parking
        }
        return Promise.reject()
    },
    updateParking : async (identifiant, parking) => {
        try {
            const park = await prisma.parking.update({
                where: {
                    identifiant: identifiant,
                },
                data: parking
            })
            return new Parking(park)
        } catch (e) {
            return Promise.reject()
        }
    },
    updateParkingIfModified: async (identifiant,date, parking) => {
        try {
            await prisma.parking.updateMany({
                where: {
                    identifiant: identifiant,
                    horodatage: {lt: date}
                },
                data: parking
            })
            return await parkingDao.findParkingByIdentifiant(identifiant)
        } catch (e) {
            return Promise.reject(e)
       }
    }
}

const p = new Parking({
    identifiant: '1',
    nom: 'parkingue',
    ouvert: true,
    horodatage: new Date(),
    nombreDePlacesDisponibles: 100,
    latitude: 47.21407529499999,
    longitude: -1.552558781000016
})
const p2 = new Parking({
    identifiant: '2',
    nom: 'VIKTOR parkingue',
    ouvert: true,
    horodatage: new Date(),
    nombreDePlacesDisponibles: 69,
    latitude: 47.21407529499999,
    longitude: -1.552558781000016
})
const p3 = new Parking({
    identifiant: '3',
    nom: 'ZUPER parkingue',
    ouvert: true,
    horodatage: new Date(),
    nombreDePlacesDisponibles: 56,
    latitude: 47.21407529499999,
    longitude: -1.552558781000016
})
const mp = new Parking({
    identifiant: '1',
    nom: 'parkingue GUEDINGUE',
    ouvert: true,
    horodatage: new Date(),
    nombreDePlacesDisponibles: 100,
    latitude: 47.21407529499999,
    longitude: -1.552558781000016
})
// Promise.all([
//     parkingDAO.addParking(p),
//     parkingDAO.addParking(p2),
//     parkingDAO.addParking(p3),
//     parkingDAO.updateParking('1', mp)
// ]).then(async () => {
//     console.log(await parkingDAO.findParkings())
// })
export {Parking, parkingDao}