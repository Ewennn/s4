'use strict'
import { PrismaClient } from '@prisma/client'
import User from '../model/user.mjs'
const prisma = new PrismaClient()



//pas de bd uniquement en mémoire
const model = new Users()
export const userDao = {
    //tous les utilisteurs
    findAll: async() => {
        try {
            const users = (await prisma.user.findMany()).map(user => new User(user))
            return users
        } catch (e) {
            return Promise.reject(e)
        }
    },
    //ajout un utilisateur
    //renvoie l'utilisateur ajouté ou null sinon
    findByLogin: async(login) => {
        try {
            const user = (await prisma.user.findUnique({ where: { login: login } }))
            return user == null ? null : new User(user)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    //supprime un utilisateur
    //renvoie l'utilisateur supprimé ou null sinon
    deleteByLogin: async(login) => {
        try {
            const user = await prisma.user.delete({ where: { login: login } })
            return new User(user)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    //ajout un utilisateur
    //renvoie l'utilisateur ajouté ou null si il était déjà présent
    add: async(user) => {
        try {
            const user = await prisma.user.create({ data: user })
            return new User(user)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    //Modifie un utilisateur
    //premd en paramètre le login du user à modifier et la modification
    //renvoie le user modifier ou null
    update: async(login, user) => {
        try {
            const userr = await prisma.user.update({
                where: {
                    login: login
                },
                data: user
            })
            return new User(userr)
        } catch (e) {
            return Promise.reject(e)
        }
    }
}