'use strict'
import { userDao } from "../dao/userDao.mjs";
export const userController = {
    findAll: async() => {
        try {
            return await userDao.findAll()
        } catch (e) {
            return Promise.reject({ message: "error" })
        }
    },
    findByLogin: async(login) => userDao.findByLogin(login),
    deleteByLogin: async(login) => userDao.deleteByLogin(login),
    add: async(user) => userDao.add(user),
    update: async(login, user) => userDao.update(login, user)
}