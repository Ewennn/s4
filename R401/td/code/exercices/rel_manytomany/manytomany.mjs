'use strict'

import { PrismaClient } from "@prisma/client"
const prisma = new PrismaClient()

function makeid(length) {
    let result = '';
    const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
        counter += 1;
    }
    return result;
}
const email = makeid(5)

await prisma.post.deleteMany({
    where: {
        id: {
            gt: 0
        }
    }
})

await prisma.user.deleteMany({
    where: {
        id: {
            gt: 0
        }
    },
})

await prisma.category.deleteMany({})

await prisma.user.create({
    data: {
        email: email + "@test.fr",
    }
})

await prisma.post.create({
    data: {
        userEmail: email + "@test.fr",
        comment: makeid(54),
        categories: {
            create: [
                {name: "Magic"}
            ]
        }
    }
})
await prisma.post.create({
    data: {
        userEmail: email + "@test.fr",
        comment: makeid(54),
        categories: {
            create: [
                {name:"Magic"}, {name:"Butterflies"}
            ]
        }
    }
})

const response2 = await prisma.post.findMany({
    include: {
        categories: true
    }
})

const response3 = await prisma.user.findMany({
    where: {
        id: {
            gt: 0
        }
    },
    include: {
        posts: {
            include: {
                categories: true
            }
        }
    }
})
console.log(response3);
console.log(response3[0].posts[1].categories);



// const response = await prisma.user.findMany({
//     where: {
//         id: {
//             gt: 0
//         }
//     },
//     select: {
//         id: true,
//         email: true,
//         posts: {
//             select: {
//                 id: true,
//                 author: true,
//                 userEmail: true,
//                 comment: true,
//                 categories: {
//                     select: {
//                         id: true,
//                         name: true
//                     }
//                 } 
//             }
//         }
//     }
// })