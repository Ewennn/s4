const etu1 = {
    nom: "Victor",
    note: 18
}
const etu2 = {
    nom: "Lénny",
    note: 18
}
const etu3 = {
    nom: "Jonathan",
    note: 20
}

const classe = {
    nom: "BUT2",
    etudiants: [etu1, etu2, etu3]
}
classe.ajouter = function(etu) {
    this.etudiants.push(etu)
}
classe.virer = function() {
    this.etudiants = []
}


export default classe