"use strict"

import events from "events"

const em = new events.EventEmitter();

em.on('firstEvent', function (data) {
    console.log(data);
})
em.on('secondEvent', function (data) {
    console.log("HALAL " + data);
})

em.emit('secondEvent', "Hello there !")
setTimeout(() => {
    em.emit('firstEvent', 10)
}, 10)