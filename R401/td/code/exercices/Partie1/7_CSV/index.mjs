"use strict"

import csv from 'csv-parser'
import fs from 'fs'
import path from 'path';
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)
const inputFile = path.resolve(__dirname, "./miam.csv")

let headerKey = ""
const results = []



fs.createReadStream(inputFile)
  .pipe(csv({separator: ";"}))
  .on('headers', (headers) => {
    headerKey = headers.find(header => header.toLowerCase().includes("partement"))
  })
  .on('data', (data) => {
    if (data[headerKey] == "Loire-Atlantique") {
      results.push([data['Commune'], data['Catégorie du restaurant'] ,data['Adresse2']])
    }
  })
  .on('end', () => {
    console.log(results);
  });