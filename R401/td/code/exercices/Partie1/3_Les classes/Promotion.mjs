import Etudiant from "./Etudiant.mjs"

class Promotion {
    nom
    etudiants = []

    constructor(nom) {
        this.nom = nom
    }

    ajouter(etu) {
        if (etu instanceof Etudiant) {
            this.etudiants.push(etu)
            return
        }
        throw new Error("On ne peut ajouter que des etudiants dans une promotion")
    }

    vider() {
        this.etudiants = []
    }
}

export default Promotion