class Humain {
    #nom

    static population = 0

    constructor(nom) {
        Humain.population++
        this.#nom = nom
    }

    get nom() {
        return this.#nom
    }
}

export default Humain