"use strict"

const Animal = function(nom) {
    this.nom = nom
}

Animal.prototype.crie = function() {
    return this.nom + " crie";
}    

const Chien = function(nom, couleur) {
    Animal.call(this, nom)
    this.couleur = couleur
}

Chien.prototype = Object.create(Animal.prototype)
Chien.prototype.constructor = Chien;

Chien.prototype.crie = function() {
    return Animal.prototype.crie.call(this) + " wawa"
}

const jojo = new Chien("Jonathan", "noir")
console.log(jojo.crie())
console.log(jojo);