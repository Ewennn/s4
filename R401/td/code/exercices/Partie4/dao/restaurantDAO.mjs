'use strict'
import { PrismaClient } from '@prisma/client'
let prisma = new PrismaClient()
import User from "../model/user.mjs";
import Restaurant from "../model/restaurant.mjs";
export const userBDDao = {
    save : async (user) => {
        try {
            const param = {...user} //clone
            delete param.restaurants
            await prisma.user.create({
                data: param
            })
            const userAdded =
                await userBDDao.findByLogin(user.login)
            return userAdded
        } catch (e) {
            return Promise.reject(e)
        }
    },
    deleteAll : async () => {
        try {
            return await prisma.user.deleteMany({})
        } catch (e) {
            return Promise.reject(e)
        }
    },
    findAll : async () => {
        try {
            return (await prisma.user.findMany({}))
                .map(obj => new User(obj) )
        } catch (e) {
            return Promise.reject(e)
        }
    },
    //renvoie l'utilisateur à partir de son login avec les restaurants
    findByLogin : async(login) => {
        try {
            const res =  (await prisma.user.findUnique({
                where : {
                    login: login
                },
                include : {
                    restaurants: true
                }
                }))
            if (res == null)
                return  null
            return  new User(res)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    //renvoie la liste des restaurants de l'utilisateur
    addRestaurant: async (userAdded, restaurantToAdd) => {
        try {
            userAdded.restaurants.push(restaurantToAdd)
            const res = await  prisma.user.update ({
                data : {
                    restaurants: {
                        create : userAdded.restaurants
                    }
                },
                where : {
                    login : userAdded.login
                }
            })
        } catch (e) {
            return Promise.reject(e)
        }
    }
}