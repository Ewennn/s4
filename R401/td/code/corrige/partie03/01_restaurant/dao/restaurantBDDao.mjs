'use strict'
import { PrismaClient } from '@prisma/client'
let prisma = new PrismaClient()

import csv from 'csv-parser'

import fs from 'node:fs'

import {parkingHTTPDao} from "./parkingHTTPDao.mjs";

import Restaurant from "../model/restaurant.mjs";

export const restaurantBDDao = {
    //parking
    populate : async (fileName) => {
        try {
            await restaurantBDDao.deleteAll()
            return await new Promise((resolve, reject) => {
                fs.createReadStream(fileName)
                    .pipe(csv({ separator: ';',
                        mapHeaders: ({ header, index }) => header.toLowerCase().trim()
                    }))
                    .on('data', async data => {
                        const restaurant = new Restaurant({
                            nomDeLOffreTouristique : data['nom de l\'offre touristique'],
                            typeDeRestaurant : data['type de restaurant'],
                            categorieDuRestaurant : data['catégorie du restaurant'],
                            adresse1: data['adresse1'],
                            adresse2: data['adresse2'],
                            adresse3: data['adresse3'],
                            codePostal: data['code postal'],
                            latitude : parseFloat(data['latitude']),
                            longitude :  parseFloat(data['longitude']),
                        })
                        try {
                            await restaurantBDDao.save(restaurant)
                        }
                        catch (e) {
                            //console.log(restaurant)
                            //reject({meesage: "error"})
                        }
                    })
                    .on('end', () => {
                        resolve(true);
                    });
            })
        } catch (e) {
            return Promise.reject(e)
        }
    },
    save : async (restaurant) => {
        try {
            const res =  await prisma.restaurant.create({data:restaurant})
            return new Restaurant(res)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    deleteAll : async () => {
        try {
            return await prisma.restaurant.deleteMany({})
        } catch (e) {
            return Promise.reject(e)
        }
    },
    findAll : async () => {
        try {
            return (await prisma.restaurant.findMany({})).map(onj => new Restaurant(obj))
        } catch (e) {
            return Promise.reject(e)
        }
    },
    findByNomDeLOffreTouristique : async (nomDeLOffreTouristique) => {
    try {
        const restaurant = await prisma.restaurant.findUnique({where : {
                nomDeLOffreTouristique: nomDeLOffreTouristique
            }})
        return restaurant
    } catch (e) {
        return Promise.reject(e)
    }
    },
    findParkingsByNomDeLOffreTouristique: async (nomDeLOffreTouristique) => {
        try {
            const restaurant = await  restaurantBDDao.findByNomDeLOffreTouristique(nomDeLOffreTouristique)
            return await parkingHTTPDao.findParkingsByCoordinates(restaurant.latitude, restaurant.longitude)
        } catch (e) {
            return Promise.reject(e)
        }
    },
    findRestaurantsByParkingId : async (identifiantParking) => {
        try {
            const parking = await parkingHTTPDao.findParkingsById(identifiantParking)
            if (parking === undefined)
                return []
            return (await restaurantBDDao
                .findAll())
                .map(restaurant => [restaurant, parking.distance(restaurant.latitude, restaurant.longitude)])
                .sort((a1,a2) => a1[1] - a2[1])
                .map(obj => obj[0])
        } catch (e) {
            return Promise.reject(e)
        }
    }
}