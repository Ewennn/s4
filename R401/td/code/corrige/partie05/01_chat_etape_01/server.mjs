import Hapi from '@hapi/hapi'
import Inert from '@hapi/inert'
import path from 'path';
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

import { Server } from 'socket.io';

const routes =[
    {
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.',
                redirectToSlash: true,
                index: true,
            }
        }
    }
]


const server =  Hapi.server({
    port: 3000,
    host: 'localhost',
    routes: {
        files: {
            relativeTo: path.join(__dirname, 'public')
        }
    }

});

const io = new Server(server.listener);


export const init = async () => {

    await server.initialize();
    return server;
};

// Global variables to hold all usernames and rooms created
let usernames = {};
let rooms = [
    { name: "global", creator: "Anonymous" },
    { name: "chess", creator: "Anonymous" },
];
export  const start = async () => {
    await server.register([
        Inert
    ]);

    server.route(routes);
    await server.start();
    console.log(`Server running at: ${server.info.uri}`);
    //socket


    io.on("connection",  (socket) => {
        console.log(`User connected to server.`);

        socket.on("createUser",  (username) => {
            socket.username = username;
            usernames[username] = username; //creation d'un attribut
            socket.currentRoom = "global";
            socket.join("global");
            console.log(`User ${username} created on server successfully.`);
            socket.emit("updateChat", "INFO", "You have joined global room");
            socket.broadcast
                .to("global")
                .emit("updateChat", "INFO", username + " has joined global room");
            io.sockets.emit("updateUsers", usernames);
            socket.emit("updateRooms", rooms, "global");
        });

        socket.on("sendMessage",  (data) => {
            io.sockets.to(socket.currentRoom).emit("updateChat", socket.username, data);
        });

        socket.on("createRoom", (room) => {
            if (room != null) {
                rooms.push({ name: room, creator: socket.username });
                io.sockets.emit("updateRooms", rooms, null);
            }
        });

        socket.on("updateRooms",  (room) => {
            socket.broadcast
                .to(socket.currentRoom)
                .emit("updateChat", "INFO", socket.username + " left room");
            socket.leave(socket.currentRoom);
            socket.currentRoom = room;
            socket.join(room);
            socket.emit("updateChat", "INFO", "You have joined " + room + " room");
            socket.broadcast
                .to(room)
                .emit(
                    "updateChat",
                    "INFO",
                    socket.username + " has joined " + room + " room"
                );
        });

        socket.on("disconnect",  () => {
            console.log(`User ${socket.username} disconnected from server.`);
            console.log(usernames)
            delete usernames[socket.username];
            console.log(usernames)
            io.sockets.emit("updateUsers", usernames);
            socket.broadcast.emit(
                "updateChat",
                "INFO",
                socket.username + " has disconnected"
            );
        });
    });


    return server;
};


process.on('unhandledRejection', (err) => {

    console.log(err);
    process.exit(1);
});