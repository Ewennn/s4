"use strict"
const etu1 = {
    nom : "etudiant1",
    note : 10
}

const etu2 = {
    nom : "etudiant2",
    note : 18
}

const etu3 = {
    nom : "etudiant3",
    note : 8
}

const classe = {
    nom : "BUT2",
    etudiants : [etu1, etu2, etu3]
}

export default classe