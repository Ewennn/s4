'use strict'
import Restaurant from "./restaurant.mjs";


export default class User {
    login
    password

    restaurants

    constructor(obj) {
        this.login = obj.login || ""
        this.password = obj.password || ""
        if (obj.restaurants)
            this.restaurants = obj
                .restaurants
                .map(restaurant => new Restaurant(restaurant))
        else
            this.restaurants = []
    }
}