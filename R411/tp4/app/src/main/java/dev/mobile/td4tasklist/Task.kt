package dev.mobile.td4tasklist

data class Task (
    var title: String = "",
    var priority: Int,
    var isCompleted: Boolean = false
) : Comparable<Task> {

    fun finish() {
        isCompleted = !isCompleted
    }

    override fun toString() =
        "$title ($priority) ${when { isCompleted -> "☑" else -> "☐"} }"

    override fun compareTo(other: Task): Int = when {
        this.priority != other.priority -> other.priority compareTo this.priority
        this.title != other.title -> other.priority compareTo this.priority
        else -> 0
    }

}