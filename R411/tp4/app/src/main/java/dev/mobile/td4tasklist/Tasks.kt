package dev.mobile.td4tasklist

class Tasks {
    companion object {

        val COMPARATOR = Comparator<Task> { t0, t1 -> t0.compareTo(t1) }
        val COMPARATOR_ALPHABETICAL = Comparator<Task> { t0, t1 -> t0.title.compareTo(t1.title) }

        fun initTaskslist(): MutableList<Task> {
            return mutableListOf<Task>(
                Task("Ne pas regarder mon téléphone", 1),
                Task("Réviser TD1 Android", 4),
                Task("Ne pas regarder mon téléphone", 2),
                Task("Améliorer mon Anglais", 2),
                Task("Réviser TD2 Android", 4),
                Task("Ne pas regarder mon téléphone", 3),
                Task("Réviser TD3 Android", 4,true),
                Task("Acheter des pâtes", 1),
                Task("Ne toujours pas regarder mon téléphone", 4),
                Task("Faire TD4 Android", 5),
                Task("Faire du sport", 3),
                Task("Dormir", 5),
                Task("Finir la saé", 2),
                Task("Utiliser John the ripper", 2),
                Task("Lire un livre", 4)
            )
        }
    }
}


