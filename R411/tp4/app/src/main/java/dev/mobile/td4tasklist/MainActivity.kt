package dev.mobile.td4tasklist

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.ListView
import android.widget.Switch
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dev.mobile.td4tasklist.Tasks.Companion.COMPARATOR
import dev.mobile.td4tasklist.Tasks.Companion.COMPARATOR_ALPHABETICAL

class MainActivity : AppCompatActivity() {

    lateinit var taskAdapter: ArrayAdapter<Task>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var taskList: MutableList<Task>
        taskList = Tasks.initTaskslist()

//        val taskAdapter = TaskAdapter(this, taskList)
//        val vue = findViewById<ListView>(R.id.list_tasks)
//        taskAdapter.getView(0, vue, )

        val taskAdapter = ArrayAdapter<Task>(this, android.R.layout.simple_expandable_list_item_1, taskList)
        val vue = findViewById<ListView>(R.id.list_tasks)
        vue.adapter = taskAdapter

        vue.setOnItemClickListener { adapterViewvue, _, i, _ ->
            val task = adapterViewvue.getItemAtPosition(i) as Task
            task.finish()
            taskAdapter.notifyDataSetChanged()
        }
    }
}