package dev.mobile.td3notes

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageButton
import android.widget.RatingBar
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity

class MainActivity : AppCompatActivity() {
    private lateinit var notes: Notes
    private lateinit var nomTv: TextView
    private lateinit var noteRb: RatingBar
    private lateinit var recule: ImageButton
    private lateinit var avance: ImageButton
    private lateinit var details: ImageButton

    val detailContrat = registerForActivityResult(DetailsContractActivity()) { result ->
        notes.courante()?.commentaire = result.commentaire
        println("RESULT: $result")
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nomTv = findViewById(R.id.nom)
        noteRb = findViewById(R.id.note)
        recule = findViewById(R.id.recule)
        avance = findViewById(R.id.avance)
        details = findViewById(R.id.detail)
        notes = Notes.init()

        afficheNote(notes.courante() ?: Note.FAKE_NOTE)

        val supprime = findViewById<ImageButton>(R.id.supprime)
        supprime.setOnClickListener {
            notes.supprimer()
            afficheNote(notes.courante())
        }
        recule.setOnClickListener{ afficheNote(notes.precedente()) }
        avance.setOnClickListener{ afficheNote(notes.suivante()) }

        details.setOnClickListener {
            val courant = this.notes.courante()
            detailContrat.launch(courant)
//            val detailsIntent = Intent(this, DetailActivity::class.java)
//            detailsIntent.putExtra("courant", courant)
//            startActivity(detailsIntent)
        }
    }

    fun afficheNote(note: Note?) {
            nomTv.text = note?.nom ?: Note.FAKE_NOTE.nom
            noteRb.rating = note?.note?.toFloat() ?: Note.FAKE_NOTE.note.toFloat()
    }

}