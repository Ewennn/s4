package dev.mobile.td3notes

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
class Note(var nom: String?, var note: Double, var commentaire: String?): Parcelable {

    override fun toString(): String {
        return "Note{" +
                "nom='" + nom + '\'' +
                ", note=" + note +
                ", commentaire='" + commentaire + '\'' +
                '}'
    }

    init {
        require(!(note < 0 || note > NOTE_MAX)) { "Illegal note: $note" }
    }

    companion object {
        const val NOTE_MAX = 5.0
        val FAKE_NOTE = Note("", 0.0, "")
    }
}
