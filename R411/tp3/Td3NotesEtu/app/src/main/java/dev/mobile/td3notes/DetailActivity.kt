package dev.mobile.td3notes

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.RatingBar
import androidx.appcompat.app.AppCompatActivity


class DetailActivity : AppCompatActivity() {

    companion object {
        val CLE = "DetailActivity"
    }

    private lateinit var backButton: Button
    private lateinit var okButton: Button
    private lateinit var textName: EditText
    private lateinit var ratingNote: RatingBar
    private lateinit var textCommentaire: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_detail)

//        Récupération des informations depuis l'intent
        val courant = intent.getParcelableExtra<Note>(CLE)
//        val courant = intent.getParcelableExtra<Note>("courant")
        println("DETAIL_ACTIVITY: $courant")

//        Récupération des éléments graphiques
        backButton = findViewById(R.id.cancel)
        okButton = findViewById(R.id.ok)

        textName = findViewById(R.id.nomi)
        ratingNote = findViewById(R.id.notei)
        textCommentaire = findViewById(R.id.commentairei)

//        Affichage des informations de l'indent dans les éléments graphiques
        textName.setText(courant?.nom)
        ratingNote.rating = courant?.note!!.toFloat()
        textCommentaire.setText(courant?.commentaire)

        okButton.setOnClickListener {
            courant.commentaire = textCommentaire.text.toString()
            val intentOK = Intent().putExtra(CLE, courant)
            setResult(RESULT_OK, intentOK)
            finish()
        }

        backButton.setOnClickListener {
            finish()
        }
    }
}
