package dev.mobile.td3notes

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.activity.result.contract.ActivityResultContract

class DetailsContractActivity: ActivityResultContract<Note, Note>() {
    override fun createIntent(context: Context, input: Note?): Intent {
        return Intent(context, DetailActivity::class.java).putExtra(DetailActivity.CLE, input)
    }

    override fun parseResult(resultCode: Int, intent: Intent?): Note {
        var res = Note("", 0.0, "")
        if (resultCode == Activity.RESULT_OK)
            res = intent?.extras?.get(DetailActivity.CLE) as Note
        return res
    }
}