# R4.11 TD 3 : Communication entre Activités


## Objectifs
passer d'une activité à une autre :
- avec ou sans transmission de données,
- avec ou sans données en retour
- explicitement ou implicitement

 ## Intent implicite

   - Réalisez une première activité contenant un champs de saisie texte et un bouton.
   - Dans le code de traitement du clic sur le bouton, en utilisant le constructeur approprié, créer un  `Intent` : 
   	1. action : `Intent.ACTION_VIEW`
    2. donnée, sous forme d'une Uri : concaténer la chaîne de caractère contenue dans le champ de saisie à `"geo:0,0?q="` puis formater le résultat en URI en utilisant la méthode `Uri.parse()`
    
   - Démarrez l'activité (sans désignation explicite de celle-ci). 
   - Testez en saisissant (par exemple) un nom de ville dans le champ de texte. 

Pour plus de détail, consulter : 

   [https://developers.google.com/maps/documentation/android-api/intents](https://developers.google.com/maps/documentation/android-api/intents)


## Intent explicite
Nous allons maintenant réaliser l'application suivante :

   ![](img/recap.png)

- Dans le projet fourni, démarrez l'activité  *Detail* au clic sur le bouton ➀.


## Intent explicite avec passage de données
- Affichez dans l'activité  *Detail*, dans les `Views` appropriées, la `Note` sélectionnée (transmettre toutes les valeurs dans l'intent).
- `OK` comme `Cancel` permettent de revenir à la première acivité.


## Passage d'objets entre activités
Nous allons maintenant passer un objet entre deux activités

- Reprenez l'exercice précédent et, au lieu de passer individuellement toutes les données, passer un objet de la classe `Note` entre les deux activités.
- Que se passe-t-il ?
- Implantez maintenant dans la classe `Note`, l’interface `android.os.Parcelable`. Pour ce faire vous avez deux solutions : soit implanter "vraiment" l'interface, soit utiliser une annotation.
    
Pour ce faire, ajoutez le plugin `Gradle` suivant au fichier `build.gradle` de votre application :
```
plugins {
    id 'kotlin-parcelize'
}
```
Vous pouvez maintenant rendre une classe parcelable facilement : 
```
import kotlinx.parcelize.Parcelize

@Parcelize
class User(val firstName: String, val lastName: String, val age: Int): Parcelable
```

voir : [https://developer.android.com/kotlin/parcelize](https://developer.android.com/kotlin/parcelize) et [https://developer.android.com/reference/android/os/Parcelable.html](https://developer.android.com/reference/android/os/Parcelable.html)


## Intent explicite avec retour
   - Dans le projet fourni, créez une troisième activité  *Add* dont la vue est également `activity_add_detail.xml`
   - cette activité est lancée au clic sur le bouton ➁.
   - l'activité  *Add* permet la saisie d'une `Note`. Deux boutons annulent ou de valident la saisie. En cas de validation (`OK`), les informations saisies sont transmises à la première activité par un extra. En cas d'annulation (`Cancel`), on retourne à la première activité.
   - En cas de validation, la `Note` saisie doit être ajouté au modèle.
   - En cas d'annulation, un `Toast` nous le signale (aucune donnée saisie).
   - vous pouvez opter pour la méthode de votre choix pour le traitement du résultat :  contrat `StartActivityForResult`, contrat personnalisé ou utiliser `startActivityForResult/onActivityResult`


## Intent implicite avec retour

- Assurez-vous d'avoir un contact disponible dans le gestionnaire de contact de votre dispositif de test.
- Vous avez noté le bouton `pick` ➂ dans l'activité  *Add* ; le clic sur ce bouton démarre le gestionnaire de contact. Ceci se fait en utilisant un `Intent` implicite :
  	+ action : `Intent.ACTION_PICK`,
  	+ type : `ContactsContract.Contacts.CONTENT_TYPE` 
- vous pouvez opter pour la méthode de votre choix pour le traitement du résultat :  contrat `StartActivityForResult`, contrat `PickContact` ou utiliser `startActivityForResult/onActivityResult`
- le choix d'un contact vous ramène en arrière en positionnant un résultat et des données.
- remplir le champ de texte avec le nom du contact. Le code pour la lecture du contact (à placer dans la méthode `onActivityResult` de l'activité  *Add* ou dans l’enregistrement du contact) est le suivant :
```
val cursor: Cursor? = contentResolver.query(contact, null, null, null, null)
	if (cursor != null && cursor.moveToFirst()) {
		val nom : String =
			cursor.getString(cursor.getColumnIndexOrThrow(ContactsContract.Contacts.DISPLAY_NAME))
		// nom est le nom du contact sélectionné
        cursor.close()
     }
```


## Pour aller plus loin
Nous allons maintenant coder une activité capable de visualiser des images.

- Créez une activité pouvant recevoir des `Intent` implicites. Pour ce faire, dotez le manifest de notre activité d'une section `intent-filter` dans le manifest. Cette section doit impérativement intégrer la catégorie `CATEGORY_DEFAULT` et bien sûr ce qui indique la capacité à visualiser (VIEW) des images.
```
<intent-filter>
  <action android:name="android.intent.action.VIEW" />
  <data android:mimeType="image/*" />
  <category android:name="android.intent.category.DEFAULT" />
</intent-filter>
<intent-filter>
      <category android:name="android.intent.category.DEFAULT"/>
      <action android:name="android.intent.action.VIEW"/>
      <data
          android:mimeType="image/*"
          android:scheme="content" />
</intent-filter>
```
- Créer une activité avec une `ImageView` (d'id `imageview`).
- Utiliser les données fournies :
```
findViewById<ImageView>(R.id.imageView).setImageURI(intent.data)
```
- Télécharger une image et par `Ouvrir avec` depuis l'application `Fichiers`de votre émulateur, votre activité devrait être proposée

source : 
[https://developer.android.com/guide/components/intents-filters.html](https://developer.android.com/guide/components/intents-filters.html)

