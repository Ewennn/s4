"use strict"
//Version courte
//Decomposable en plusieurs composants
const storeDAO = {
    getProducts: async () => {
        const res = await fetch("https://fakestoreapi.com/products")
        const data = await res.json()
        return data
    }

}

const { useState, useEffect } = React;
function App(){
    const [loading, setLoading] = useState(false)
    const [data, setData] = useState(null)

    useEffect(() => {
        setLoading(true)
        storeDAO.getProducts()
            .then(data => {
                console.log(data)
                setData(data)
            })
            .catch(err => console.dir(err))
            .finally(() => setLoading(false))
    }, [])

    return (
        <section>
            <h1>Fake Shop API response:</h1>
            {loading && "Loading..."}
            {!!data && data.length > 0 ? data.map((product) => {
                return(
                    <article key={product.id}>
                        <h2>name: {product.title}</h2>
                    </article>
                )
            }):(<p>API did not provided any product, try again.</p>)
            }
        </section>
    )
}

const root = ReactDOM.createRoot(document.querySelector('#app_container'));
root.render(
  <App/>
);
