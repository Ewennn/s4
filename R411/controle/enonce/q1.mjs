"use strict"
const storeDAO = {
    getCategories : async () => {
        const res = await fetch("https://fakestoreapi.com/products/categories")
        const data = await res.json()
        return data
    },
    getFromCategorie : async (categorie) => {
        const res = await fetch("https://fakestoreapi.com/products/category/"+categorie)
        let data = []
        try {
            data = await res.json()
        } catch (e){}
        return data
    },
    getProduct: async (id) => {
        const res = await fetch("https://fakestoreapi.com/products/"+id)
        const data = await res.json()
        return data
    }
}

const dataList = document.querySelector("#categories")
const input = document.querySelector("#categorie")
const categories = await storeDAO.getCategories()
const images = document.querySelector(".row")



categories.forEach(categorie => {
    const option = document.createElement("option")
    option.value=categorie
    dataList.appendChild(option)
})

const draw = (products) => {
    images.innerHTML=""
    const columns = []
    for (let i=0; i< 4; i++) {
        columns[i]=document.createElement("div")
        columns[i].className="column"
        images.appendChild(columns[i])
    }

    products.forEach((product,pos) => {
            const divContainer = document.createElement("div")
            const divContent = document.createElement("div")
            divContainer.appendChild(divContent)
            const img = document.createElement("img")
            img.setAttribute("alt",product.id)
            img.setAttribute("src", product.image)
            img.addEventListener("click",async () =>{
                    const productFromStore = await storeDAO.getProduct(product.id)
                    document.querySelectorAll(".column > div > div")
                        .forEach(elt => elt.innerHTML="")
                    divContent.innerHTML = `
                        <p> ${productFromStore.title}</p>
                        <p> ${productFromStore.price}</p>`

            })
            divContainer.appendChild(img)
            columns[pos%4].appendChild(divContainer)
        }
    )
}
input.addEventListener("change",async ()=>{
    const products = await storeDAO.getFromCategorie(input.value)
    if (products!=[]){
        draw(products)
    }

})
