"use strict"
const txt1 = "Fake Shop API response:"
const txt2 = "Loading..."

const storeDAO = {
    getProducts: async () => {
        const res = await fetch("https://fakestoreapi.com/products")
        const data = await res.json()
        return data
    },
}

// Sélection des élements DOM
const section = document.querySelector("#site-content")
const loadDiv = document.createElement("h1")
loadDiv.innerHTML = txt2
const titleDiv = document.createElement("h1")
titleDiv.innerHTML = txt1

// Chargement
section.appendChild(loadDiv).firstChild

// Récupération des données
const products = storeDAO.getProducts().then(res =>{
    console.log(res)

    // Affichage des données dans des balises différentes
    res.forEach(product => {
        const container = document.createElement("article")
        const title = document.createElement("h2")
        container.appendChild(title)
        title.innerHTML = "name: " + product.title

        const section = document.querySelector("#site-content")
        section.appendChild(container)
    });
}).then(() => {
    // Mise à jour des titres
    section.removeChild(loadDiv)
    section.prepend(titleDiv)
    
})