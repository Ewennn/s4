"use strict"
//Version courte
//Decomposable en plusieurs composants
const storeDAO = {
    getCategories : async () => {
        const res = await fetch("https://fakestoreapi.com/products/categories")
        const data = await res.json()
        return await data
    },
    getFromCategorie : async (categorie) => {
        const res = await fetch("https://fakestoreapi.com/products/category/"+categorie)
        let data = []
        try {
            data = await res.json()
        } catch (e){}
        return data
    },
    getProduct: async (id) => {
        const res = await fetch("https://fakestoreapi.com/products/"+id)
        const data = await res.json()
        return data
    }
}

const categories = []
storeDAO.getCategories().then(res => {
    res.forEach(it => {
        categories.push(it)
    })
})

class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {categories: categories, products:[], product:null}
    }


    render(){

        // console.log(this.state.categories);
        // this.state.categories.forEach(it => console.log(it))

        const options = this.state.categories.map(categorie =>
            <option value={categorie} key={categorie}> </option>
        )

        const imgTags = this.state.products.map(product => {

                return <div key={product.id}>
                    <div>
                        {this.state.product!=null && this.state.product.id == product.id &&
                        <p> {product.title} </p>}
                        {this.state.product!=null && this.state.product.id == product.id &&
                            <p> {product.price} </p>}

                    </div>
                    <img alt={product.id} src={product.image} />
                </div>
            }
                )
        const imgs = [[],[],[],[]]
        imgTags.forEach((img,i) => imgs[i%4].push(img))


        return (
        <div>
            <div className="header">
                <h1> Select a categorie </h1>
                <p>
                    <input list="categories" />
                    <datalist id="categories">
                        {options}
                    </datalist>
                </p>
            </div>
            <div className="row">
                <div className="column"> {imgs[0]} </div>
                <div className="column"> {imgs[1]} </div>
                <div className="column"> {imgs[2]} </div>
                <div className="column"> {imgs[3]} </div>
            </div>
        </div>)

    }

    componentDidMount() {
        console.log(this.state.categories);
    }
}

const root = ReactDOM.createRoot(document.querySelector('#app_container'));
root.render(
  <App/>
);
