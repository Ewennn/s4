package dev.mobile.td2convertisseurtemperature

import android.os.Bundle
import android.view.KeyEvent
import android.widget.EditText
import android.widget.ImageButton
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatActivity


class MainActivity : AppCompatActivity() {
    private lateinit var temp: Temperature

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        temp = Temperature(0)

        val textCel = findViewById<EditText>(R.id.celsius)
        val textFar = findViewById<EditText>(R.id.fahrenheit)

        val toFarButton = findViewById<ImageButton>(R.id.celToFar)
        val toCelButton = findViewById<ImageButton>(R.id.farToCel)

        val celBar = findViewById<SeekBar>(R.id.seekBarCel)
        val farBar = findViewById<SeekBar>(R.id.seekBarFar)

//        Actions de la barre de temperatures en Celsius
        celBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                temp.celsius = p1
                textCel.setText(p1.toString())
                farBar.progress = temp.fahrenheit
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })

//        Actions de la barre de temperatures en Fahrenheit
        farBar.setOnSeekBarChangeListener(object: SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(p0: SeekBar?, p1: Int, p2: Boolean) {
                temp.fahrenheit = p1
                textFar.setText(temp.fahrenheit.toString())
                celBar.progress = temp.celsius
            }

            override fun onStartTrackingTouch(p0: SeekBar?) {}

            override fun onStopTrackingTouch(p0: SeekBar?) {}

        })

        textCel.setText(this.temp.celsius.toString())
        textFar.setText(this.temp.fahrenheit.toString())

//        Reset du TextEdit
        textCel.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) textCel.text.clear()
        }
        textFar.setOnFocusChangeListener { _, hasFocus ->
            if (hasFocus) textFar.text.clear()
        }


        textCel.setOnKeyListener { view, i, keyEvent ->
            if (keyEvent.keyCode == KeyEvent.KEYCODE_ENTER) {
                this.temp.celsius = textCel.text.toString().toInt()
                farBar.progress = temp.fahrenheit
                celBar.progress = temp.celsius
                textFar.setText(this.temp.fahrenheit.toString())
            }
            true
        }
        textFar.setOnKeyListener { view, i, keyEvent ->
            if (keyEvent.keyCode == KeyEvent.KEYCODE_ENTER) {
                this.temp.fahrenheit = textFar.text.toString().toInt()
                farBar.progress = temp.fahrenheit
                celBar.progress = temp.celsius
                textCel.setText(this.temp.celsius.toString())
            }
            true
        }



//        Conversion de la temperature Celsius to fahrenheit
        toFarButton.setOnClickListener {
            if (textCel.text.isBlank()) {
                textFar.setText("")
            } else {
                this.temp.celsius = textCel.text.toString().toIntOrNull() ?: 0
                celBar.progress = this.temp.celsius
                farBar.progress = this.temp.fahrenheit
//                textFar.setText(this.temp.fahrenheit.toString())
            }
        }

//        Conversion de la temperature Fahrenheit to Celsius
        toCelButton.setOnClickListener {
            if (textFar.text.isBlank()) {
                textCel.setText("")
            } else {
                this.temp.fahrenheit = textFar.text.toString().toIntOrNull() ?: 0
                celBar.progress = this.temp.celsius
                farBar.progress = this.temp.fahrenheit
//                textCel.setText(this.temp.celsius.toString())
            }
        }
    }

}