package dev.mobile.td2convertisseurtemperature

class Temperature(  // Stockée en Celsius
    private var temperature: Int
) {
    var celsius: Int
        get() = temperature
        set(temperature) {
            this.temperature = temperature
        }
    var fahrenheit: Int
        get() = (temperature.toDouble() * 9 / 5 + 32).toInt()
        set(temperature) {
            this.temperature = ((temperature.toDouble() - 32) * 5 / 9).toInt()
        }
}