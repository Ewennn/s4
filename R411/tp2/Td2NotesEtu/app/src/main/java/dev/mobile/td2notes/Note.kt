package dev.mobile.td2notes

class Note(var nom: String?, var note: Double, var commentaire: String?) {

    override fun toString(): String {
        return "Note{" +
                "nom='" + nom + '\'' +
                ", note=" + note +
                ", commentaire='" + commentaire + '\'' +
                '}'
    }

    init {
        require(!(note < 0 || note > NOTE_MAX)) { "Illegal note: $note" }
    }

    companion object {
        const val NOTE_MAX = 5.0
        val FAKE_NOTE = Note("", 0.0, "")
    }

}
