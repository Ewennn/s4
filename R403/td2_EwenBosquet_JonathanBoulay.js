// Bosquet Ewen & Jonathan Boulay

//1
db.restaurants.find()
db.getCollection('restaurants').find()

//2
db.restaurants.find({cuisine: "American"})

//3
db.restaurants.find({cuisine: "American"}, {_id: false})

//4
db.restaurants.find({cuisine: "American"}).count()

//5
db.restaurants.find({cuisine: "American"}, {_id: false}).limit(10)

//6
db.restaurants.find().limit(1)

//7
db.restaurants.find().limit(1).pretty()     //deprecated

//8
db.restaurants.find({"address.building": "123"})

//9
db.restaurants.find({"address.building": {$nin: [2780, 2790]}})

//10
db.restaurants.find({"address.building": {$exists: true}}).count()

//11
db.restaurants.find({"address.building": {$exists: false}}).count()

//12
db.restaurants.find({"address.building": {$nin: ["123", "1236"]}}, {_id: 0, name: 1}).count()

//13
db.restaurants.find({"address.building": {$nin: ["8825"]}}).count()

//14
db.restaurants.find({restaurant_id: "40356018"})

//15
db.restaurants.updateOne({restaurant_id: "40356018"},{$push:
    {grades:
        {date: new ISODate("10/06/2022"),
        grade: "B",
        score: new NumberInt(10)}}
    }
)

//16
db.restaurants.updateOne({restaurant_id: "40356018"}, {$pull:
    {"grades.score": "B"}
})

//17
db.restaurants.updateOne({restaurant_id: "40356018"}, {$addToSet:
{
    grades: [{grade: "B"}, {grade: "C"}]
}
})

//18
db.restaurants.find({"grades.grade": {$all: ["B", "C"]}})

//19
db.restaurants.find({grades: {$size: 5}})

//20
db.restaurants.find({"grades.grade": {$gte: "B", $lte: "D"}}).count()

//21
db.restaurants.find({"grades.0.grade": "A"})

//22
db.restaurants.aggregate(
    [
        {$sort: {name: 1}}
    ]
)

//23
db.restaurants.aggregate(
    [
        {$sort: {name: -1}}
    ]
)

//24
db.restaurants.aggregate(
    [
        {$sort: {name: 1, restaurant_id: -1}}
    ]
)

//25
db.restaurants.find(
    {$expr:
        {$gt: [
            {$strLenCP: '$cuisine'}, 20
            ]
        }
    }
)

//26
db.restaurants.aggregate(
    [
        {$match: {cuisine: "Hamburgers"}}
    ]
)

//27
db.restaurants.aggregate(
[
    {$match: {cuisine: "Hamburgers"}},
//    {$match: {"address.building": {$gt: "70"}}},
    {$match: {name: {$regex: /^M/}}}
]
)

//28
db.restaurants.aggregate([
    {$addFields: {
        sumGrades: {$sum: "$grades.score"}
    }},
    {$project:{
        _id: 0,
        name: 1,
        sumGrades: 1
    }},
    {$sort: {
        sumGrades: -1
    }}
])

//29
db.restaurants.aggregate([
    {$addFields: {
        moyGrades: {$avg: "$grades.score"}
    }},
    {$project:{
        _id: 0,
        name: 1,
        moyGrades: 1,
    }},
    {$sort: {
        moyGrades: -1
    }}
])

//30
db.restaurants.aggregate([
{
    $group: { _id: null, uniqueValues: {$addToSet: "$address.building"} },
}])

db.restaurants.distinct("address.building")

//31
db.restaurants.distinct("address.building").length

db.restaurants.aggregate([
    {$group: {
        _id: "$address.building",
        count: {$sum: 1}
    }},
    {$sort: {
        count: -1
    }}
])

//32
db.restaurants.aggregate([
    {$group: {
        _id: "$address.zipcode",
        count: {$sum: 1}
    }},
    {$sort: {
        count: -1
    }}
])

//33
db.restaurants.aggregate([
    {$project: {
        _id: "$name",
        countGrades: {$size: "$grades"}
    }},
    {$sort: {
        countGrades: -1
    }}
])

//34
db.restaurants.aggregate([
    {$group: {
        _id: "$cuisine",
        countCuisine: {$sum: 1}
    }},
    {$sort: {
        countCuisine: -1
    }}
])

//35
db.restaurants.aggregate([
    {$group: {
        _id: {$size: "$grades"},
        countGrades: {$sum: 1}
    }}
])

//36
db.restaurants.aggregate([
    {$match: {"grades.grade": "A"}},
    {$project: {
        _id: 0,
        restaurant_id: 1,
        name: 1,
        allGrades: {
            $filter: {
                input: "$grades",
                as: "grade",
                cond: {$ne: ["$$grade.grade", "A"]}
                }
            }
        }
    }
])