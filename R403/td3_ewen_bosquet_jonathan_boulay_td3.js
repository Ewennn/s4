/*TD3*/
/*Jonathan Boulay, Ewen Bosquet*/


db.vendeur.find({})
/*1*/

/*produit*/

db.produit.insertMany([{
 "_id" : NumberInt(1),
 "libelle" : "chou",
 "prixU" : NumberInt(5),
 "qte" : NumberInt(7),
 "dateFab" : ISODate("2022-03-01T08:00:00.000+0000")
},
{
 "_id" : NumberInt(2),
 "libelle" : "tomate",
 "prixU" : NumberInt(10),
 "qte" : NumberInt(2),
 "dateFab" : ISODate("2021-03-01T08:00:00.000+0000")
},
{
 "_id" : NumberInt(3),
 "libelle" : "oignon",
 "prixU" : NumberInt(20),
 "qte" : NumberInt(1),
 "dateFab" : ISODate("2021-03-01T09:00:00.000+0000")
},
{
 "_id" : NumberInt(4),
 "libelle" : "salade",
 "prixU" : NumberInt(5),
 "qte" : NumberInt(10),
 "dateFab" : ISODate("2021-03-15T09:00:00.000+0000")
},
{
 "_id" : NumberInt(5),
 "libelle" : "tomate",
 "prixU" : NumberInt(5),
 "qte" : NumberInt(20),
 "dateFab" : ISODate("2021-04-04T11:21:39.736+0000")
},
{
 "_id" : NumberInt(6),
 "libelle" : "chou",
 "prixU" : NumberInt(10),
 "qte" : NumberInt(10),
 "dateFab" : ISODate("2021-04-04T21:23:13.331+0000")
},
{
 "_id" : NumberInt(7),
 "libelle" : "oignon",
 "prixU" : 7.5,
 "qte" : NumberInt(5),
 "dateFab" : ISODate("2022-06-04T05:08:13.000+0000")
},
{
 "_id" : NumberInt(8),
 "libelle" : "tomate",
 "prixU" : 7.5,
 "qte" : NumberInt(10),
 "dateFab" : ISODate("2022-09-10T08:43:00.000+0000")
},
{
 "_id" : NumberInt(9),
 "libelle" : "tomate",
 "prixU" : NumberInt(10),
 "qte" : NumberInt(5),
 "dateFab" : ISODate("2023-02-06T20:20:13.000+0000")
}])


/*vendeur*/

db.vendeur.insertMany([{'_id':'f1','nom':'Alfred','statut':20,'ville':'Londres',
 'ventes':[
 { '_id': 1, 'qte': 7, 'date': new Date('2022-03-01T08:00:00Z') },
 { '_id': 2, 'qte': 2, 'date': new Date('2021-03-01T08:00:00Z') }
 ]
 },
 {'_id':'f2','nom':'Robert','statut':10,'ville':'Paris',
 'ventes':[
 { '_id': 3, 'qte': 1, 'date': new Date('2021-03-01T09:00:00Z') },
 { '_id': 4, 'qte': 10, 'date': new Date('2021-03-15T09:00:00Z')},
 { '_id': 1, 'qte': 2, 'date': new Date('2022-05-16T10:00:00Z')},
 { '_id': 7, 'qte': 4, 'date': new Date('2022-03-01T09:00:00Z') },
 ]
 },
 {'_id':'f3','nom':'Raymonde','statut':30,'ville':'Paris',
 'ventes':[
 { '_id': 2, 'qte': 20, 'date': new Date('2021-04-04T11:21:39.736Z') },
 { '_id': 1, 'qte': 10, 'date': new Date('2021-04-04T21:23:13.331Z') },
 { '_id': 4, 'qte': 7, 'date': new Date('2022-10-23T21:23:13.331Z') },
 { '_id': 5, 'qte': 12, 'date': new Date('2022-09-21T21:23:13.331Z') },
 ]
 },
 {'_id':'f4','nom':'Gaston','statut':20,'ville':'Londres',
 'ventes':[
 { '_id': 3, 'qte': 5, 'date': new Date('2022-06-04T05:08:13Z') },
 { '_id': 2, 'qte': 10, 'date': new Date('2022-09-10T08:43:00Z') }
 ]
 },
{'_id':'f5','nom':'Hector','statut':30,'ville':'Nantes',
 'ventes':[
 { '_id': 2, 'qte': 5, 'date': new Date('2023-02-06T20:20:13Z') }
 ]
}])

/*2*/
db.vendeur.find({})

/*3*/
db.vendeur.find({}).sort({"nom":1, "_id":-1})

/*4*/
db.vendeur.find({},{"nom":1, "ville" : 1, "_id":0})

/*5*/
db.vendeur.find({"_id":"f4"},{"nom":1, "statut":1, "ville" : 1, "_id":0})

/*6*/
db.vendeur.find({"ville" : "Paris"})

/*7*/
db.vendeur.find({ville: {$in : ["Londres", "Paris"]}})

/*8*/
db.vendeur.find({"ville" : "Paris", "statut" :{$gte:10, $lte : 20}})

/*9*/
db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $group: {
      _id: null,
      totalVentes: {
        $sum: "$ventes.qte"
      }
    }
  }
])

/*10*/

db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $group: {
      _id: "$_id",
      totalVentes: {
        $sum: "$ventes.qte"
      }
    }
  }
])

/*11*/

db.vendeur.find({
})

db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $match: {
      "ventes._id": 3
    }
  },
  {
    $group: {
      _id: "$_id",
      totalVentes: {
        $sum: "$ventes.qte"
      }
    }
  }
])

/*12*/


db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $match: {
      "ventes.date": {
    $gte: ISODate("2021-04-04T11:00:00Z"),
    $lt: ISODate("2021-04-04T12:00:00Z")
  }
    }
  }
])


/*13*/
db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $match: {
      "ventes.date": {
    $gte: ISODate("2021-04-04T11:00:00Z")
  }
    }
  },
  {
      $project: {
          nom : "$nom",
          ville : "$ville",
          ventes : "$ventes",
          qte : "$ventes.qte"
      }
  }
])


/*14*/

db.vendeur.find({"ventes._id" : {$all : [2,3]}}).count()

/*db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $match: {
      "ventes._id": {
    $all: [2,3]
  }
    }
  },
  {
      $project: {
          nom : "$nom",
          ville : "$ville",
          ventes : "$ventes",
          qte : "$ventes.qte"
      }
  }
])*/



/*15*/

db.vendeur.find({"ventes" : {$size : 2}}).count()

/*db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $match: {
      "ventes": {
    $size: 2
  }
    }
  },
  {
      $project: {
          nom : "$nom",
          ville : "$ville",
          ventes : "$ventes",
          qte : "$ventes.qte"
      }
  }
])*/

/*16*/
db.vendeur.find({
})

db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $group: {
      _id: "$nom",
      totalVentes: {
        $sum: "$ventes.qte"
      }
    }
  },
  {
      $project:{
          _id:0,
          nom:"$_id",
          totalVentes : 1
      }
  }
])

/*17*/

db.vendeur.aggregate([{
    $group : {
        _id : "$ville"
    }},
    {
        $project:{
            _id:0,
            ville : "$_id"
        }
    }
])

db.vendeur.distinct("ville")

/*18*/


db.vendeur.aggregate([{
    $group : {
        _id: "$ville",
        count: { $sum : 1 }
    }
}])


/*19*/

db.vendeur.count()

/*20*/

db.vendeur.aggregate([
  {
    $unwind: "$ventes"
  },
  {
    $lookup: {
      from: "produit",
      localField: "ventes._id",
      foreignField: "_id",
      as: "produit"
    }
  },
  {
    $match: {
      "produit.libelle": "chou"
    }
  },
  {
    $project: {
      _id: 0,
      nom: 1
    }
  }
])

/*21*/

db.produit.bulkWrite([
  {
    updateMany: {
      filter: { libelle: "tomate" },
      update: { $set: { couleur: "rouge" } }
    }
  },
  {
    updateMany: {
      filter: { libelle: "brocoli" },
      update: { $set: { couleur: "vert" } }
    }
  },
  {
    updateMany: {
      filter: { libelle: "chou" },
      update: { $set: { couleur: "vert" } }
    }
  },
  {
    updateMany: {
      filter: { libelle: "salade" },
      update: { $set: { couleur: "vert" } }
    }
  },
  {
    updateMany: {
      filter: { libelle: "oignon" },
      update: { $set: { couleur: "jaune" } }
    }
  }
]);

/*22*/

db.vendeur.aggregate([
  {
    $lookup: {
      from: "produit",
      localField: "ventes._id",
      foreignField: "_id",
      as: "produit"
    }
  },
  {
    $match: {
      "produit.couleur": {$ne: "rouge" }
    }
  },
  {
    $project: {
      _id: 0,
      nom: 1,
      id : "$ventes._id"
    }
  }
])

/*23*/

db.vendeur.aggregate([
  {
    $lookup: {
      from: "produit",
      localField: "ventes._id",
      foreignField: "_id",
      as: "produit"
    }
  },
  {
    $match: {
      "produit" :  {$not : {$elemMatch : {"couleur" :{$ne : "rouge"}}}}
    }
  },
  {
    $project: {
      _id: 0,
      nom: 1,
      id : "$ventes._id"
    }
  }
])

/*24  Pas réussi*/




db.vendeur.aggregate([
  {
    $lookup: {
      from: "produit",
      localField: "ventes._id",
      foreignField: "_id",
      as: "produits"
    }
  },
  {
    $unwind: "$ventes"
  },
  {
    $match: { "produits.couleur" : {$not : {$nin: ["vert"]}} }
  },
  {
    $project: {
      _id: "$nom",
      total: { $sum: "$ventes.qte" }
    }
  }
]);


/*25 */


db.vendeur.aggregate([
  {
    $lookup: {
      from: "produit",
      localField: "ventes._id",
      foreignField: "_id",
      as: "produits"
    }
  },
  {
    $unwind: "$ventes"
  },
  {
    $match: { "produits.couleur" : {$not : {$nin: ["vert"]}} }
  },
  {
    $group: {
      _id: "$nom"
    }
  }
]);


/*26*/

db.vendeur.aggregate([
  {
    $lookup: {
      from: "produit",
      localField: "ventes._id",
      foreignField: "_id",
      as: "produits"
    }
  },
  {
    $unwind: "$ventes"
  },
  {
    $match: { "produits.couleur" : {$in : [1,4,6]} }
  },
  {
    $group: {
      _id: "$nom"
    }
  }
]);