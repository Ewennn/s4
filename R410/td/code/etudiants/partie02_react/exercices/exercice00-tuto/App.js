"use strict"

class Game extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            players: ["p1", "p2"],
            actual: null,
        }
    }

    render() {
        return (
            <div className="center">
                <Board
                    onClick={() => {
                        this.setState({actual: this.updateGame()})
                    }}
                    value={this.state.actual}
                />
            </div>
        )
    }

    updateGame() {
        if(this.state.actual == null || this.state.actual == "p2") {
            this.state.actual = "p1"
        } else {
            this.state.actual = "p2"
        }
        return this.state.actual
    }
}

class Board extends React.Component {
    constructor(props) {
        super(props)
        
        this.state = {
            squares: Array(9).fill(null),
            player: null
        }
    }
    renderSquare(i) {
        return (
        <Square 
            value={this.state.squares[i]}
            onClick={() => {
                this.setState({squares: this.modifySquares(i)})
            }
            }
        />
        )
    }
    modifySquares(i) {
        if (this.state.squares[i] == null) {
            this.state.squares[i] = i
        } else {
            this.state.squares[i] = null
        }
        return this.state.squares
    }

    render() {
        const status = `Prochain joueur : ${this.props.value}`

        return ( 
            <div onClick={this.props.onClick}>
                <h4 className="status">{status}</h4>
                <div className="board-row">
                    {this.renderSquare(0)}
                    {this.renderSquare(1)}
                    {this.renderSquare(2)}
                </div>
                <div className="board-row">
                    {this.renderSquare(3)}
                    {this.renderSquare(4)}
                    {this.renderSquare(5)}
                </div>
                <div className="board-row">
                    {this.renderSquare(6)}
                    {this.renderSquare(7)}
                    {this.renderSquare(8)}
                </div>
            </div>
        )
    }
}

class Square extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            value: null
        }
    }
    render() {
        return (
            // SetState re déclenche la méthode render()
            <button className="square" 
            onClick={() => this.props.onClick()}
            >
                { this.props.value } 
            </button>
        )

    }
}

const root = ReactDOM.createRoot(document.querySelector('#app_container'));
root.render(<Game />)