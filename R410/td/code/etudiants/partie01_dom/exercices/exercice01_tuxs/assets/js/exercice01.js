"use strict";
window.addEventListener("DOMContentLoaded",function(){

    const images = document.querySelectorAll('div#images img');
    const image = document.querySelector("div#image img");

    images.forEach(item =>
        item.addEventListener("click",  () => {
            image.setAttribute("src",item.getAttribute("src"));
    }))
});