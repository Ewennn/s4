"use struct"
import {movieDAO} from "./dao/movieDAO.mjs";
import {imgURL} from "./data/const.mjs";
window.addEventListener("DOMContentLoaded",async () => {
    const queryString = window.location.search;
    const urlParams = new URLSearchParams(queryString);
    const id = urlParams.get('id')
    if (id == null) {
        document.location.href='./home.html'
    }
    const refreshPopular = document.querySelector("#refreshPopular")
    const contentSection = document.querySelector('section.content')

    refreshPopular.addEventListener('click',() => {
        document.location.href='./home.html'
    })
    const movie = await movieDAO.getById(id)

    const article = document.createElement('article')
    article.classList.add("movie")

    const img = document.createElement('img')
    img.setAttribute("alt","")
    img.setAttribute('src' , imgURL+movie.poster_path)
    article.appendChild(img)

    const div = document.createElement('div')
    const h1 = document.createElement('h1')
    h1.innerText = movie.title
    div.appendChild(h1)
    const divInfo = document.createElement('div')
    divInfo.innerText=`${movie.release_date} ${Math.floor(movie.runtime / 60)}h${movie.runtime%60} ${movie.vote_average}`
    div.appendChild(divInfo)

    const divGenre = document.createElement('div')
    let s= ""
    movie.genres.forEach(genre => s+= genre.name+", ")
    s= s.substring(0,s.length-2)
    divGenre.innerHTML=s
    div.appendChild(divGenre)

    const h2 =  document.createElement('h2')
    h2.innerHTML = movie.tagline
    div.appendChild(h2)

    const divOverview =document.createElement('div')
    divOverview.innerHTML = movie.overview
    div.appendChild(divOverview)

    article.appendChild(div)
    contentSection.appendChild(article)
})