"use strict";
window.addEventListener('DOMContentLoaded', function() {
    const modale = document.querySelector("#myModal");
    const bouton = document.querySelector("#myBtn");
    const span = document.querySelector(".close");

    bouton.addEventListener('click', function() {
        modale.classList.add("displayBlock");
    });

    // When the user clicks on <span> (x), close the modal
    span.addEventListener('click', function() {
        modale.classList.remove("displayBlock");
    });

    // When the user clicks anywhere outside of the modal, close it
    document
        .querySelector('body')
        .addEventListener('click',  function(event) {
        if (event.target == modale) {
            modale.classList.remove("displayBlock");
        }
    });

});