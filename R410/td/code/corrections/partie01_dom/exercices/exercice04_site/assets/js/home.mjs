'use strict'
import {movieDAO} from "./dao/movieDAO.mjs";
import {imgURL} from './data/const.mjs';

const displayMovies = (data, html) => {
    html.innerHTML=''
    data.results.forEach(film => {
        const article = document.createElement('article')
        const img = document.createElement('img')
        img.setAttribute('alt',"")
        if (film.backdrop_path != null)
            img.setAttribute('src' , imgURL+film.backdrop_path )
        else
            img.setAttribute('src' , './assets/img/notFound.png' )
        const  a  = document.createElement('a')
        a.setAttribute('href',`movie.html?id=${film.id}`)
        a.appendChild(img)
        const divInfos  = document.createElement('div')
        const divInfo1  = document.createElement('div')
        divInfo1.innerHTML= film.release_date
        const divInfo2  = document.createElement('div')
        divInfo2.innerHTML = film.title
        const divInfo3  = document.createElement('div')
        divInfo3.innerHTML = film.vote_average
        divInfos.append(divInfo1,divInfo2,divInfo3)

        a.appendChild(divInfos)
        article.appendChild(a)

        html.appendChild(article)
    })
}


window.addEventListener("DOMContentLoaded",
    (event) => {
    const refreshPopular = document.querySelector("#refreshPopular")
    const contentSection = document.querySelector('section.content')
    const input = document.querySelector('#search')


        refreshPopular.addEventListener('click', async () =>{

            const data= await movieDAO.getPopulars()
            input.value=""
            displayMovies(data, contentSection)


        })
        refreshPopular.click()
        input.addEventListener('keyup', async() => {
            if (input.value.length > 0) {
                const data = await movieDAO.find(input.value)
               displayMovies(data, contentSection)
            }
        })
    })