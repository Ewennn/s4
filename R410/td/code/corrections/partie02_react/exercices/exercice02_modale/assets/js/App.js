class Button extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.onToggle()
    }
    render() {
        return (
            <button id="myBtn" onClick={this.handleClick}>Open Modal</button>
        )
    }
}
class Modale extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        this.props.onToggle()
    }

    render() {
        return (
            <div className={(this.props.active)?"modal displayBlock":"modal"}>
            <div className="modal-content">
            <span className="close" onClick={this.handleClick}>&times;</span>
            <img alt="" src="assets/images/image01.jpg"/>
                <p> une belle image </p>
            </div>
            </div>)
    }
}
class App extends React.Component {
    constructor(props) {
        super(props)
        this.onToggle = this.onToggle.bind(this)
        this.state= {active: false}
    }
    onToggle() {
        this.setState({active: !this.state.active})
    }

    render() {
        return (
            <div>
                <Button onToggle={this.onToggle}/>
                <Modale active={this.state.active} onToggle={this.onToggle} />
            </div>
        )
    }

}



const root = ReactDOM.createRoot(document.querySelector('#app_container'));
root.render(
    <App />);