class ImageCible extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <img src={this.props.src} alt=""/>
        )
    }
}

class ImageClickable extends React.Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick(e) {
        const imgName =
       this.props.onSelectedImage(e.target.src
           .split('/').pop().split('.')[0])
    }

    render() {
        return (
            <img src={this.props.src} alt="" onClick={this.handleClick}/>
        )
    }
}

class App extends React.Component {
    constructor(props) {
        super(props);
        this.images = ["nowhite_tux", "pax_tux"]
        this.state = {image : "nowhite_tux"}
        this.onSelectedImage = this.onSelectedImage.bind(this);
    }

    onSelectedImage(image) {
        this.setState({image: image})
    }
    render() {
        return (
            <div>
                <div id="images">
                    <ImageClickable src={"./assets/images/"+this.images[0]+".png"}
                                    onSelectedImage={this.onSelectedImage}/>
                    <ImageClickable src={"./assets/images/"+this.images[1]+".png"}
                                    onSelectedImage={this.onSelectedImage}/>
                </div>
                <div id="image">
                    <ImageCible src={"./assets/images/"+this.state.image+".png"}/>
                </div>
            </div>
        )
    }
}



const root = ReactDOM.createRoot(document.querySelector('#app_container'));
root.render(
    <App />);