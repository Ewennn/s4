const key = 'b116821a3811b251e489968a5b45422b'
const baseURL = 'https://api.themoviedb.org/3'
const imgURL = 'https://image.tmdb.org/t/p/w1280'

const movieDAO = {
    getPopulars : async (page=1) => {
        const suffix = `/movie/popular?api_key=${key}&language=en-US&page=${page}`
        const res  = await fetch(baseURL+suffix)
        const data = await res.json()
        return data
    },
    find : async (term, page=1) =>
    {
        const suffix = `/search/movie?api_key=${key}&language=en-US&query=${term}&page=${page}&include_adult=false`
        const res = await fetch(baseURL + suffix)
        const data = await res.json()
        return data
    }
}

class Popular extends  React.Component {
    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        this.props.onUpdate()
    }

   render(){
        return <button onClick={this.handleClick}> Popular movies </button>
   }
}

class Query extends  React.Component {
    constructor(props) {
        super(props)
        this.handleKeyUp = this.handleKeyUp.bind(this)
    }

    handleKeyUp(e){
      this.props.onUpdate(e.target.value)
    }

    render(){
        return <input onKeyUp={this.handleKeyUp}/>
    }
}

class Article extends React.Component {
    constructor(props) {
        super(props)

    }

    render(){
        return (
            <article>
                <a href={"movie.html?id="+this.props.movie.id}>
                    <img alt=""

                         src={this.props.movie.backdrop_path?imgURL+"/"+this.props.movie.backdrop_path:"./assets/img/notFound.png"}/>
                        <div>
                            <div>{this.props.movie.release_date}</div>
                            <div>{this.props.movie.title}</div>
                            <div>{this.props.movie.vote_average}</div>
                        </div>
                    </a>
            </article>
        )
    }
}
class App extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            query : "",
            movies : []
        }
        this.doUpdate = this.doUpdate.bind(this)
    }
    componentDidMount() {
        this.doUpdate()
    }

    doUpdate(query){
        if (query != undefined)
            this.setState({query: query})

        if (this.state.query == "")
            movieDAO.getPopulars()
                .then(data => {

                    this.setState({movies: data.results})
                })
        else
            movieDAO.find(query)
                .then(data => {
                    console.log('+',this.state.query+'+', data.results.map(obj => obj.id))
                    this.setState({movies: data.results})
                })
    }


    render() {
        const articles = this.state.movies.map(movie => {
            return <Article key={movie.id} movie={movie}/>
        })

        return (
            <div className="container">
                <header>
                    <Popular onUpdate={this.doUpdate}/>
                    <Query onUpdate={this.doUpdate}/>
                </header>
                <section className="content">
                    {articles}
                </section>
            </div>
        )
    }

}



const root = ReactDOM.createRoot(document.querySelector('#app_container'));
root.render(
    <App />);