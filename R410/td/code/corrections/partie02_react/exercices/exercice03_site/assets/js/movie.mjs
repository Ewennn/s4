const key = 'b116821a3811b251e489968a5b45422b'
const baseURL = 'https://api.themoviedb.org/3'
const imgURL = 'https://image.tmdb.org/t/p/w1280'

const movieDAO = {
    getById : async (id) =>
    {
        const suffix = `/movie/${id}?api_key=${key}&language=en-US`
        const res = await fetch(baseURL + suffix)
        const data = await res.json()
        return data
    }
}


class Popular extends  React.Component {
    constructor(props) {
        super(props)
        this.handleClick = this.handleClick.bind(this);
    }
    handleClick() {
        document.location.href="home.html"
    }

    render(){
        return <button onClick={this.handleClick}> Popular movies </button>
    }
}
class App extends React.Component {
    constructor(props) {
        super(props)
        const queryString = window.location.search;
        const urlParams = new URLSearchParams(queryString);
        const id = urlParams.get('id')

        if (id == null) {
            document.location.href='./home.html'
        }
        this.id = id
        this.state = {
            movie: null
        }
    }

    componentDidMount() {
       movieDAO.getById(this.id).then(movie => this.setState({
           movie: movie
       }))
    }
    render(){
        let article =""
        if (this.state.movie!=null) {

            let genre =""
            this.state.movie.genres.forEach(currentGenre => genre+= currentGenre.name+", ")
            genre= genre.substring(0,genre.length-2)

            article = <article className="movie">
                <img alt=""
                     src={imgURL+"/"+this.state.movie.poster_path}/>
                <div>
                    <h1> {this.state.movie.title} </h1>
                    <div>{this.state.movie.release_date+" "+Math.floor(this.state.movie.runtime / 60)+"h"+this.state.movie.runtime%60+" "+this.state.movie.vote_average}</div>
                    <div> {genre} </div>
                    <div> {this.state.movie.tagline}
                    </div>
                </div>
            </article>
        }

        return(
            <div className="container">
            <header>
                <Popular/>
            </header>
                <section className="content">
                    {article}
                </section>
        </div>)
    }

}

const root = ReactDOM.createRoot(document.querySelector('#app_container'));
root.render(
    <App />);