package iut.r412.automata;

import iut.r412.automata.utils.Pair;

import java.util.ArrayList;
import java.util.List;

public class Automata<T> {

    private final ArrayList<State<T>> states;
    private final List<T> alphabet;
    private final State<T> init;
    private final State<T> last;

    public Automata(List<State<T>> states, List<T> alphabet, Pair<State<T>, State<T>> borders) {
        this.states = new ArrayList<>(states);
        this.alphabet = alphabet;
        this.init = borders.getFirst();
        this.last = borders.getSecond();
        this.states.add(0, init);
        this.states.add(last);

    }

    public void addTransition(State<T> from, State<T> to, T... transition) {
        if (this.states.contains(from) && this.states.contains(to)) {
            for (T t : transition) {
                from.addTransition(t, to);
            }
        }
    }

    public boolean accept(T goal) {
        return false;
    }

//    public boolean accept(Automata<?> automate, String goal) {
//
//    }

    @Override
    public String toString() {
        return "Automata{" +
                "\n\tstates=" + states +
                ", \n\talphabet=" + alphabet +
                ", \n\tinit=" + init +
                ", \n\tlast=" + last +
                "\n}";
    }
}
