package iut.r412.automata;

import iut.r412.automata.utils.Pair;

import java.util.List;

public class Appli {
    public static void main(String[] args) {
        State<Character> init = new State<>("E0");
        State<Character> e1 = new State<>("E1");
        State<Character> e2 = new State<>("E2");
        State<Character> e3 = new State<>("E3");
        State<Character> last = new State<>("E4");
        List<Character> alphabet = List.of(':', ')', '(');

        Automata<Character> automata = new Automata<>(
                List.of(e1, e2, e3),
                alphabet,
                Pair.of(init, last)
        );

        automata.addTransition(init, e1, ':');
        automata.addTransition(init, e2, ';', ']');
        automata.addTransition(e1, last, '-', '=');
        automata.addTransition(e1, e2, ')', '(');
        automata.addTransition(e2, last, '-');

        System.out.println(automata);

        automata.accept('e');
    }
}
