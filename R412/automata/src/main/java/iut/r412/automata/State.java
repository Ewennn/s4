package iut.r412.automata;

import java.util.HashMap;

public class State<T> {
    private final String name;
    private HashMap<T, State<T>> outGoingTram = new HashMap<>();

    public State(String name) {
        this.name = name;
    }

    public void addTransition(T transition, State<T> state) {
        this.outGoingTram.put(transition, state);
    }

    @Override
    public String toString() {
        return "State{" +
                "name='" + name + '\'' +
                ", outGoingTram=" + outGoingTram +
                '}';
    }
}
