package palindrome

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test

internal class VerifPalindromeTest {

    var vp = VerifPalindrome()

    @Test
    fun testVide() {
        assertTrue(vp.isPalindrome(""))
    }

    @Test
    fun test2() {
        assertFalse(vp.isPalindrome("bougnoul"))
    }

    @Test
    fun testKayak() {
        assertTrue(vp.isPalindrome("kayak"))
    }
}