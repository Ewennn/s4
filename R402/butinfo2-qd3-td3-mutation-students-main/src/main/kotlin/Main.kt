import palindrome.VerifPalindrome

fun main(args: Array<String>) {
    println("Hello World!")

    val pv = VerifPalindrome()

    var mot ="Bonjour"
    println("$mot "+pv.isPalindrome(mot))
    mot = "LOL"
    println("$mot "+pv.isPalindrome(mot))
}