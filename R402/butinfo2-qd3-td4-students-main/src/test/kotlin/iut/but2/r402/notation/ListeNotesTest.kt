package iut.but2.r402.notation

import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.assertThrows
import org.junit.jupiter.api.fail

internal class ListeNotesTest {

    @Test
    fun testMoyenne1() {
        assertEquals(5F, ListeNotes(intArrayOf(5)).moyenne())
        assertEquals(0F, ListeNotes(intArrayOf()).moyenne())
        assertEquals(10F, ListeNotes(intArrayOf(9, 11)).moyenne())
    }

    @Test
    fun testNombreOccurrence1() {
       assertThrowsExactly(IllegalArgumentException::class.java) {
           ListeNotes(intArrayOf()).nombreOccurrence(-5)
       }
        assertThrowsExactly(IllegalArgumentException::class.java) {
            ListeNotes(intArrayOf()).nombreOccurrence(24)
        }
        assertEquals(1, ListeNotes(intArrayOf(5)).nombreOccurrence(5))
        assertEquals(0, ListeNotes(intArrayOf(5)).nombreOccurrence(6))
        assertEquals(0, ListeNotes(IntArray(0)).nombreOccurrence(5))

        assertEquals(1, ListeNotes(intArrayOf(20)).nombreOccurrence(20))
        assertEquals(1, ListeNotes(intArrayOf(0)).nombreOccurrence(0))
    }
}