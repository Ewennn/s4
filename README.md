# Matières

- R401 - Architecture logicielle
- R402 - Qualité de développement
- R403 - Qualité et au-delà du rationnel 
- R404 - Méthodes d'optimisation
- R405 - Anglais
- R406 - Communication interne
- R407 - Projet personnel et professionel
- R408 - Virtualisation (non enseigné)
- R409 - Management avancé des systèmes d'information
- R410 - Complément Web
- R411 - Développement pour applications mobiles
- R412 - Automates et languages
- SAE401 - Développement d'une application complexe
- P401 - Démarche portfolio