#!/bin/bash
cd s4/
if [[ `git status --porcelain` ]]; then
    git add .
    git commit -m "Auto commited changes at $(date +"%d/%m/%Y %H:%M:%S")"
    git pull
fi
if [[ `git log origin/main..HEAD` ]]; then
    git push
fi
git pull